/*
 * playground.c
 *
 * Created: 5/Jul/2020 3:21:20 PM
 *  Author: Motti
 */ 
#include "clock_config.h"
#include <util/delay.h>

#include <stdint.h>
#include <usart_basic_example.h>
#include <usart_basic.h>
#include "main.h"
#include "heating.h"
#include "timing.h"
#include "RxTxTasks.h"

#define	ENABLE_CHARGING()	nEN_CHG_set_level(0)

#define	ENABLE_CHARGING()	nEN_CHG_set_level(0)
#define	DISABLE_CHARGING()	nEN_CHG_set_level(1)
#define	USB_CONNECTED		!nPPR_get_level()
#define	CHARGING			!nCHG_get_level()

#define	SET_MOTOR()			HAPTIC_set_level(1)
#define	CLEAR_MOTOR()		HAPTIC_set_level(0)


void testDelay(void)
{
	while(1){
	LED1_toggle_level();
		_delay_ms(500);
	}
}

void displayReset(void)
{
	for(uint8_t i=0; i<1; i++)
	{
		SET_RED();
		_delay_ms(100);
		CLEAR_RED();
		SET_BLUE();
		_delay_ms(100);
		CLEAR_BLUE();
		SET_GREEN();
		_delay_ms(100);
		CLEAR_GREEN();
		_delay_ms(100);
	}
}

void clearAllLEDs(void)
{
	CLEAR_RED();
	CLEAR_GREEN();
	CLEAR_BLUE();
	CLEAR_LED1();
}

void test_Switches_LEDs(void)
{
	while(1)
	{
		clearAllLEDs();
		_delay_ms(10);
		if(CONTROL_get_level()) SET_RED();
		if(!CONTROL_get_level()) SET_GREEN();
		if(DI_HEAT_get_level()) SET_BLUE();
		if(!DI_HEAT_get_level()) SET_LED1();
		_delay_ms(1);
		WDT_0_init();
	}
}

void test_charging(void)
{
	initPWM();
	while(1)
	{
		LED1_set_level(nCHG_get_level());
		nEN_CHG_set_level(CONTROL_get_level());
		RED_set_level(!CONTROL_get_level());
		WDT_0_init();
	}
}

void test_pwm(void)
{
	initPWM();
	while(1)
	{
		PWM_0_enable();
		_delay_ms(500);
		_delay_ms(500);
		PWM_0_disable();
		_delay_ms(500);
		_delay_ms(500);
		WDT_0_init();
	}
}

uint8_t buffer[2] = {0xFF, 0xFF};
void test_temperature(void)
{
	uint32_t bits_16 = 0;
	volatile uint32_t temperature = 0;
	
	initSPI();
	
	while(1)
	{
//		fakeSPI();
		SS_THERMOCOUPLE_set_level(0);
		SPI_0_read_block(buffer, sizeof(buffer));
		while(!SPI_0_status_done()){}
		SS_THERMOCOUPLE_set_level(1);
	
		bits_16 = buffer[0] << 8 | buffer[1];
		temperature = bits_16 >> 3;
		temperature /= 4;
		
		_delay_ms(10);
		WDT_0_init();
	}
}

void initDevice(void)
{
	initPWM();
	init_DI_HEAT_ISR();
	heating_task_init();
	RxTxInit();
	initControl();
	initSPI();
//	init_SWI();
}
	
void Dragon_play()
{
//	displayReset();

	memcpy(&HEATER_A.flash, &default_flash, sizeof(HEATER_A.flash));		// FOR TESTING PURPOSES ONLY
	
	initDevice();
	
//	testDelay();
//	test_pwm();
//	test_Switches_LEDs();
//	test_charging();
//	test_temperature();
//	testWriteMicrochipEEPROM();
	
	while(1)
	{
		heatingProcess();
 		tx_task();
 		rx_task();
 		_delay_ms(100);
		WDT_0_init();
	}
}