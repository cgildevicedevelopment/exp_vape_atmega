/*
 * ISR.c
 *
 * Created: 07/07/2020 17:16:26
 *  Author: Administrator
 */ 

#include <pwm_basic.h>
#include <atmel_start.h>
#include <util/delay.h>

#define DI_HEAT_RISING_EDGE		DI_HEAT_get_level()
#define DI_HEAT_FALLING_EDGE	!DI_HEAT_get_level()

#define PWM_0_CB_RATE	50


static volatile uint8_t callback_count = 0;


// ---------------------------------------- CONTROL ----------------------------------------
void init_CONTROL_ISR(void)
{
	CONTROL_set_isc(PORT_ISC_BOTHEDGES_gc);
}

ISR(PORTA_PORT_vect)
{
	// PD7 interrupt flag
	if(PORTA.INTFLAGS == PORT_INT3_bm)
	{	
		_delay_ms(10);						// Debaounce
		
		PORTA.INTFLAGS |= PORT_INT3_bm;		// Clear interrupt flag
		
		setControlType(CONTROL_get_level());
	}
}

// ---------------------------------------- DI_HEAT ----------------------------------------
void init_DI_HEAT_ISR(void)
{
	DI_HEAT_set_isc(PORT_ISC_BOTHEDGES_gc);
}

ISR(PORTD_PORT_vect)
{
	// PD7 interrupt flag
	if(PORTD.INTFLAGS == PORT_INT7_bm)
	{
		_delay_ms(10);						// Debaounce
		
		PORTD.INTFLAGS |= PORT_INT7_bm;		// Clear interrupt flag
		
		if(DI_HEAT_RISING_EDGE){
			startHeating();
		}
		
		else if(DI_HEAT_FALLING_EDGE){
			stopHeating();
		}
	}
}

// ---------------------------------------- PWM ----------------------------------------

ISR(TCA0_CMP2_vect)
{
	// Clear the interrupt flag
	TCA0.SINGLE.INTFLAGS = TCA_SINGLE_CMP2_bm;
	
	if (++callback_count >= PWM_0_CB_RATE) {
//		callback_count = 0;
		
		heat_cb_low();
	}
}

void TCA0_OVF_CB(void)
{
	// CHECK ON SCOPE
	// CPU = 20MHz, CLK = 10MHz, tick = 0.1us
	// PER = 50,000 OVF when CNT = PER = 5ms, F = 200Hz
	// callback = 100, OVF after 500ms
	
	if (callback_count >= PWM_0_CB_RATE) {
		callback_count = 0;
		
		heat_cb_high();
	}
}

void setPWMdutyCycle(uint16_t duty_cyle)
{
	// Top = 50,000, 1% = 500
	const uint16_t ticks_one_percent = 500;
	
	PWM_0_load_duty_cycle_ch2(duty_cyle * ticks_one_percent);
}


void initPWM(void)
{
	PWM_0_register_callback(TCA0_OVF_CB);				// Set the callback function address for TCA Overflow
 	TCA0.SINGLE.CTRLB &= 0b11111000;					// Clear the PWM mode configuration
 	TCA0.SINGLE.CTRLB |= 3;								// Set the PWM mode to single slope
 	TCA0.SINGLE.INTCTRL |= (1 << TCA_SINGLE_CMP2_bp);	// Enable Compare interrupt (Outputs PWM on PD2)							 __        __
 	G_HTR_set_inverted(1);								// Invert to start high (P-Channel) and duty cycle is +ve e.g. 25% Duty =   |  |______|  |______
	G_HTR_set_dir(PORT_DIR_OUT);						// Set pin to output
//	setPWMdutyCycle(100);
//	TCA0.SINGLE.CTRLA |= 1;								// Start Timer
}
