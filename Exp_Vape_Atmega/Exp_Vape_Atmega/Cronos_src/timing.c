/*
 * timing.c
 *
 * Created: 31/Jul/2019 10:36:59 AM
 *  Author: Motti
 */ 
#include "driver_init.h"

//static struct timer_task TIMER_0_time;
static uint32_t time_since_start = 0;

void increment()
{
	time_since_start++;
}
// static void increment_time_cb(const struct timer_task *const timer_task)
// {
// 	time_since_start++;
// }
// 
// 
// void init_global_timer()
// {
// 	time_since_start = 1;
// 	
// 	TIMER_0_time.cb = increment_time_cb;
// 	TIMER_0_time.interval = 10;
// 	TIMER_0_time.mode = TIMER_TASK_REPEAT;
// 	
// 	timer_add_task(&TIMER_0, &TIMER_0_time);
// 	
// 	timer_start(&TIMER_0);
// }

uint32_t getGlobalTime()
{
	return time_since_start;
}

ISR(TCB0_INT_vect)
{
	increment();

	TCB0.INTFLAGS = TCB_CAPT_bm;
}