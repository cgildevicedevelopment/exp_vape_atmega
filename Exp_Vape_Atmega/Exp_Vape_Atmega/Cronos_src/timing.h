/*
 * timing.h
 *
 * Created: 31/Jul/2019 10:23:07 AM
 *  Author: Motti
 */ 


#ifndef TIMING_H_
#define TIMING_H_

typedef struct TaskTimming
{
	uint32_t previous_time;
	uint16_t sleep_time;
}TaskTimming;

void increment();
uint32_t getGlobalTime();
void init_global_timer();

#endif /* TIMING_H_ */