/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file or main.c
 * to avoid loosing it when reconfiguring.
 */

#include "RxTxTasks.h"
#include "atmel_start.h"
#include "timing.h"
#include "MIN_protocol.h"
#include "control.h"
#include <string.h>

#define FW_VERSION "3.1"
#define UART_PROTOCOL_

bool TX_on = false;
bool is_cdcd_enabled = false;

TaskTimming tx_timming = { .sleep_time = 20 };
TaskTimming rx_timming = { .sleep_time = 0 };


struct tx_vars global_tx_vars ;
struct headers send_packet;
struct min_context min_context1;
//struct crc32_context t;
extern struct heating_packet_t HEATER_A;

void RxTxInit()
{	
#ifdef UART_PROTOCOL_
	min_init_context(&min_context1, 0);
#endif // UART_PROTOCOL_
}

uint8_t calculate_cheksum(uint8_t *data, uint8_t size)
{
	uint8_t checksum = data[0];
	for(int i = 1; i < size; i++){
		checksum = checksum ^ data[i];
	}
	return checksum; 
}

void sendFlashtoGUI(void)
{
// 	static struct rx_packet_t rx_packet;
// 	
// 	rx_packet.local_rx_vars = HEATER_A.flash;
// 	
// 	rx_packet.local_rx_vars.R_setpoint = (HEATER_A.flash.R_setpoint - 10000) / 100;
// 	rx_packet.local_rx_vars.dry_setpoint = (HEATER_A.flash.dry_setpoint - 10000) / 100;
// 	
// 	rx_packet.header = TRANSMIT_FLASH_TO_GUI;
// 	rx_packet.message_size = sizeof(rx_packet.local_rx_vars);
// 	rx_packet.checksum = calculate_cheksum((uint8_t *)(&rx_packet.local_rx_vars),sizeof(rx_packet.local_rx_vars));
// 	
// 	tx_write(((uint8_t *)(&rx_packet)), sizeof(rx_packet));
}

void extract_packet(uint8_t* in_buffer1)
{
	struct rx_packet_t rx_packet;
	rx_packet.stop_byte = 0b11111110;
	
	///error checking
	switch (*in_buffer1)
	{		
		case TX_ON:
			TX_on = *(in_buffer1+2);
		break;
						
		case TRANSMIT_FW_VERSION:
 			send_packet.header = TRANSMIT_FW_VERSION;
 			send_packet.message_size = sizeof(FW_VERSION);
 			strcpy(send_packet.names, FW_VERSION);
 			send_packet.checksum = calculate_cheksum((uint8_t *)(FW_VERSION), sizeof(FW_VERSION));
 			send_packet.stop_byte = 0b11111110;
 			tx_write((uint8_t *)(&send_packet), sizeof(send_packet));
		break;
					
		case TRANSMIT_TX_HEADERS:
			send_packet.header = TRANSMIT_TX_HEADERS;
			strncpy(send_packet.names, "Time_(ms), Is_heating, Voltage, Thermo_Couple?, Is_Charging, t1, Resistance, Cold_resistance, Ratio, Power, error, P, I, D, target, Duty, temperature \n Duty, Ratio, Power, temperature", sizeof(send_packet.names));
			send_packet.message_size = sizeof(send_packet.names);
			send_packet.stop_byte = 0b11111110;
			send_packet.checksum = calculate_cheksum((uint8_t *)send_packet.names, sizeof(send_packet.names));
			tx_write((uint8_t *)(&send_packet), sizeof(send_packet));
		break;
					
		case TRANSMIT_FLASH_HEADERS:
			send_packet.header = TRANSMIT_FLASH_HEADERS;
			strncpy(send_packet.names, "Max_Power, Set_point, Dry_Setpoint, T_Setpoint, Kp, Ki, Kd", sizeof(send_packet.names));
			send_packet.message_size = sizeof(send_packet.names);
			send_packet.stop_byte = 0b11111110;
			send_packet.checksum = calculate_cheksum((uint8_t *)send_packet.names, sizeof(send_packet.names));
			tx_write((uint8_t *)(&send_packet), sizeof(send_packet));
		break;
					
		case TRANSMIT_FLASH_TO_GUI:
			sendFlashtoGUI();
		break;
					
		case RECEIVE_FLASH_FROM_GUI:  // updates all or single value
			memcpy((uint8_t*)&rx_packet, (uint8_t*)(in_buffer1), sizeof(struct rx_packet_t));
			//writeSettings(&rx_packet.local_rx_vars);
		break;
		
		case ENABLE_BOOTLOADER:
			//script_init_uid();
// 					boot_tag[0] = 0xffffffff;
// 					flash_write(&FLASH_0 ,0x5ffc, (uint8_t*)boot_tag, 4);
// 					_reset_mcu();
		break;
					
// 		case TOGGLE_CHARGING_VIA_GUI:
// 			//toggleGUIcharge(*(in_buffer1+2));
// 		break;
// 					
		case RESET_LOW_RES:
			resetLowRes();
		break;
		
		case TOGGLE_HEATING_VIA_GUI:
			if(*(in_buffer1+2)){
				startHeating();
			}
			else{
				stopHeating();
			}
		break;
					
		case TOGGLE_CONTROL_VIA_GUI:
			setControlType(*(in_buffer1+2));
		break;
					
		case TRANSMIT_SD_DATA:
		break;
					
		case ERASE_SD:
		break;
	}
}

int8_t tx_write(uint8_t *buf, uint32_t size)
{
#ifdef UART_PROTOCOL_
	min_send_frame(&min_context1, 2, buf, size);
#else
	//cdcdf_acm_write(buf, size);
#endif // UART_PROTOCOL_

	//delay_ms(1);
		
	return 0;
}

uint8_t tx_task()
{
	
	if(getGlobalTime() < tx_timming.previous_time + tx_timming.sleep_time)
		return false;

	tx_timming.previous_time = getGlobalTime();
	 
	struct tx_packet sending_packet;
	uint16_t sendBytes = sizeof(sending_packet);
	
	sending_packet.stop_byte = 0b11111110;
		
	
 	global_tx_vars.heating_on = heating_on;
 	global_tx_vars.millis = getGlobalTime();
 	global_tx_vars.voltage = vbat;
 	global_tx_vars.control_type = getControlType();
// 	global_tx_vars._is_charging = is_charging();
 
 	sending_packet.local_tx_vars = global_tx_vars;
 	sending_packet.heating_tx_vars = HEATER_A.tx;//getHeatingTx();

	if ( TX_on)
	{
		sending_packet.header = THIS_IS_A_TX_PACKET;
		sending_packet.message_size = sizeof(struct tx_vars) + sizeof(struct heating_tx_t);
		sending_packet.checksum = calculate_cheksum((uint8_t *)(&sending_packet.local_tx_vars),sizeof(sending_packet.local_tx_vars));
								
		tx_write(((uint8_t *)(&sending_packet)), sendBytes);
			
		return true;
	}
	
	return false;
}

void rx_task()
{
	uint8_t buff[1];
	uint8_t offset = 0;
	if(getGlobalTime() < rx_timming.previous_time + rx_timming.sleep_time)
		return;
		
	rx_timming.previous_time = getGlobalTime();
// 	if(USART_0_is_rx_ready())
// 	{
// 		
// 		uint8_t dat =  USART_0_read();
// 		
// 		USART_0_write(dat);
// 	}
	
	while(USART_0_is_rx_ready()){
		buff[offset++] = USART_0_read();
		
		if(offset > 0)
		{
			min_poll(&min_context1, buff, offset);
			offset =0;
		}
	}
	
	if(offset != 0)
	{
		min_poll(&min_context1, buff, offset);
			offset =0;
	}
	
	
		
// 	if(is_cdcd_enabled == false && cdcdf_acm_is_enabled())
// 	{
// 		cdcd_acm_configure();
// 		is_cdcd_enabled = true;
// 	}
// 	else if(is_cdcd_enabled == true && !cdcdf_acm_is_enabled())
// 	{
// 		is_cdcd_enabled = false;
// 	}
// 		
// 	if(!is_packet_queueu_empty())
// 	{
// 		in_buffer = dequeue();
// #ifdef UART_PROTOCOL_			
// 		min_poll(&min_context1, in_buffer, 64);
// #else
// 		extract_packet(in_buffer);
// #endif // UART_PROTOCOL_	
//	}
}