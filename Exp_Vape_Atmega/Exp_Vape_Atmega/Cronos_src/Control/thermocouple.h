/*
 * thermocouple.h
 *
 * Created: 24/08/2020 15:33:43
 *  Author: Administrator
 */ 


#ifndef THERMOCOUPLE_H_
#define THERMOCOUPLE_H_


uint16_t getTemperature(void);
void clear_SPI_IF(void);
void initSPI(void);


#endif /* THERMOCOUPLE_H_ */