/*
 * Control.h
 *
 * Created: 06/07/2019 14:11:18
 *  Author: Administrator
 */ 


#ifndef CONTROL_H_
#define CONTROL_H_


#define RESISTANCE_CONTROL		0
#define THERMOCOUPLE_CONTROL	1

void setControlType(uint8_t flag);
uint8_t getControlType(void);
void resetControl(struct heating_packet_t *heaterPtr);
int32_t CS_PIDragon(struct heating_packet_t *heaterPtr);

extern uint8_t control_type;

#endif /* CONTROL_H_ */