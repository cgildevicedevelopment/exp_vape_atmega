/*
 * SPI.c
 *
 * Created: 06/08/2019 16:48:50
 *  Author: Administrator
 */ 


#include <stdio.h>
#include <string.h>
#include <atmel_start.h>
#include <spi_basic_example.h>
#include <spi_basic.h>
#include <atomic.h>
#include <util/delay.h>

//static uint8_t buffer[16] = "data";
static uint8_t read_bytes[2] = {0xFF, 0xFF};

void fakeSPI(void)
{	
	SCK_set_level(0);
	while(1)
	{
		SS_THERMOCOUPLE_set_level(0);
 		for(uint8_t i=1;i<=32;i++)
 		{
 				SCK_set_level(i%2);
				 _delay_us(5);
		}
		SS_THERMOCOUPLE_set_level(1);
		_delay_ms(10);
		WDT_0_init();
	}
}

uint16_t readTemperature(void)
{
	uint16_t bits_16 = 0;
	volatile uint16_t temperature = 0;
	
	SS_THERMOCOUPLE_set_level(0);
	SPI_0_read_block(read_bytes, sizeof(read_bytes));
// 	while(!SPI_0_status_done()){}
// 	SS_THERMOCOUPLE_set_level(1);		// Cleared in interrupt
	
	bits_16 = read_bytes[0] << 8 | read_bytes[1];
	temperature = bits_16 >> 3;
	
	return temperature/4;
}

uint16_t getTemperature(void)
{
	static uint16_t temperature = 0;
	const uint16_t n_max = 3;			// Temperature doesn't update if reads too quick
	static uint16_t n = n_max;
	
	if(n == n_max){
		temperature = readTemperature();
		n = 0;
	}
	n++;
	
	return temperature;
}

void SPI_Finished(void)
{
	SS_THERMOCOUPLE_set_level(1);	// Turn off Slave Select
	SPI0.INTFLAGS;					// Clear the SPI IF by reading the IF and data registers
	SPI0.DATA;
}

void initSPI(void)
{
//	SPI0.CTRLB |= 4;						// Disable the Slave Select line when operating as SPI Master
	SPI_0_register_callback(SPI_Finished);	// Should be done by hardware - not happening
}
