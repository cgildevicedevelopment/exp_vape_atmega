/*
 * heating.h
 *
 * Created: 1/28/2018 10:35:25 AM
 *  Author: eliezers
 */ 


#ifndef HEATING_H_
#define HEATING_H_

//#include "program_config.h"

#include <atmel_start.h>

extern uint32_t vbat, res, lowRes, rat;
extern uint8_t heating_on;


/* -------------------- HEATING PACKET -------------------- */

struct __attribute__ ((__packed__)) heating_flash_t
{
	int32_t power_max;
	int32_t R_setpoint;
	int32_t dry_setpoint;
	int32_t T_setpoint;
	int32_t Kp;
	int32_t Ki;
	int32_t Kd;
};

struct __attribute__ ((__packed__)) heating_local_t
{
	uint32_t pwm_channel;
	int32_t duty_max;
	int32_t adc;
	int32_t connected;
	int32_t error_prev;
	int32_t steady_state;
};

struct __attribute__ ((__packed__)) heating_tx_t
{
	int32_t res;
	int32_t lowRes;
	int32_t rat;
	int32_t power_actual;
	int32_t error;
	int32_t P;
	int32_t I;
	int32_t D;
	int32_t power_target;
	int32_t duty;
	int32_t temperature;
};

struct __attribute__ ((__packed__)) heating_packet_t
{
	struct heating_flash_t flash;
	struct heating_local_t locals;
	struct heating_tx_t tx;
};

extern struct heating_packet_t HEATER_A;
extern struct heating_flash_t default_flash;

/* -------------------------------------------------------- */


struct rx_heating_vars
{
	uint32_t start;
	uint32_t on_time;
};

extern struct rx_heating_vars global_rx_heating_vars;

void ADCinit(void);

uint32_t getResistance(uint32_t adc_value, uint8_t pod_connected);
uint32_t getVoltage(uint32_t adc_value);

void setDutyCycle(float power);

void heatingInit(uint32_t heat_period_ms);
void resetLowRes(void);
void heatingProcess(void);
void heating_task_init();
void toggleGuiHeat(uint8_t flag);
uint8_t heatingOn(void);

void setButtonOn(void);
void setToggleOn(void);

uint32_t getButtonPressed(void);
#endif /* HEATING_H_ */