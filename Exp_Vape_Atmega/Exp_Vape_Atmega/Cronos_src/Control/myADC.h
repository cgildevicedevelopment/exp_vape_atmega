/*
 * myADC.h
 *
 * Created: 04/07/2019 02:07:27
 *  Author: Administrator
 */ 


#ifndef MYADC_H_
#define MYADC_H_

//#include "program_config.h"

#define AN_HEATER_1	ADC_MUXPOS_AIN3_gc
#define AN_BATTERY	ADC_MUXPOS_DACREF_gc

//uint16_t getADC(struct heating_packet_t *heaterPtr);
uint8_t podConnected(uint16_t res);

uint32_t getRatio(uint32_t res, uint32_t lowRes);
uint32_t getDryPuff(uint32_t rat, uint32_t dry_setpoint);
uint32_t measureVoltage(void);
#endif /* MYADC_H_ */
