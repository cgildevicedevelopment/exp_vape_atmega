/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

//#include "hal_adc_sync.h"

// #include "driver_examples.h"
// #include "driver_init.h"
// #include "utils.h"

#include "heating.h"
#include "RxTxTasks.h"
//#include "program_config.h"
#include "myADC.h"
#include "Control.h"
#include "isr.h"
#include "thermocouple.h"

#include "timing.h"

struct heating_flash_t default_flash = {.power_max=20000, .R_setpoint=12100, .dry_setpoint=19900, .T_setpoint=180, .Kp=100, .Ki=2, .Kd=0};
struct heating_packet_t HEATER_A;

struct heating_packet_t *HEATER;

const uint32_t duty_max = 95;
const uint32_t duty_min = 2;
//uint32_t R_setpoint = 12000;	// Mark10 = 10200, Lake = 12100, Joule = 10390
uint32_t dry_puff = 0;

const uint32_t task_sleep_time = 100;		// Default task sleep time
uint32_t time_since_puff = 50000;
uint32_t puff_start_time = 0;
uint32_t puff_end_time = 0;
uint32_t heating_task_timing = 0;

uint32_t low_bat = 0;
uint32_t low_bat_threshold = 3000;

uint32_t vbat = 0;
uint32_t v_temp = 0;
uint8_t heating_on = 0;

uint32_t t1 = 0;
uint32_t t2 = 0;

// ============================== TIME ==============================

void calculatePuffTime(void)
{
	if(!HEATER->locals.connected)
		return;
		
	puff_end_time = getGlobalTime();
	time_since_puff = 0;
//#ifdef One_WIRE_EEPROM
	// todo - move to another function -- this is called from an interrupt function.
//	update_puff_length(puff_end_time - puff_start_time);
//#endif
}

// ============================== POWER ==============================

uint32_t getV2(uint32_t vbat)
{
	return vbat * vbat;
}

uint32_t getPowerActual(uint32_t duty_cycle, uint32_t V2, uint32_t res)
{
	return ((duty_cycle * V2) / res) / 100;		// W = D(V^2)/R
}

uint32_t getDutyCycle(struct heating_packet_t *heaterPtr, uint32_t V2)
{
	uint32_t duty = 0;
	uint32_t duty_cycle = 0;
	
	duty = (uint32_t)(100 * ((float)heaterPtr->tx.power_target * (float)heaterPtr->tx.res) / (float)V2);	// D = WR/(V^2)
	
	if(duty > duty_max){
		duty_cycle = duty_max;
	}
	else if(duty < duty_min){
		duty_cycle = duty_min;
	}
	else{
		duty_cycle = duty;
	}
	
	return duty_cycle;
}
	
void temperatureControl(struct heating_packet_t *heaterPtr, uint32_t V2)
{
//	dry_puff = getDryPuff(heaterPtr->tx.rat, heaterPtr->flash.dry_setpoint);
	if(dry_puff)
	{
		stopHeating();
		return;
	}
	heaterPtr->tx.power_actual = getPowerActual(heaterPtr->tx.duty, V2, heaterPtr->tx.res);
#ifdef RUEVEN_HEATING
	heaterPtr->tx.power_target = heaterPtr->flash.power_max;
#else
	heaterPtr->tx.power_target = CS_PIDragon(heaterPtr);
#endif
	heaterPtr->tx.duty = getDutyCycle(heaterPtr, V2);
	
	PWM_POWER_set(heaterPtr);
}

// ============================== RESISTANCE ==============================

void calculateResRat(struct heating_packet_t *heaterPtr)
{
//	heaterPtr->res_prev = heaterPtr->res;
	heaterPtr->locals.connected = podConnected(heaterPtr->locals.adc);
	heaterPtr->tx.res = getResistance(heaterPtr->locals.adc, heaterPtr->locals.connected);
	
	if(heaterPtr->locals.connected){
		heaterPtr->tx.rat = getRatio(heaterPtr->tx.res, heaterPtr->tx.lowRes);
	}
	else{
		heaterPtr->tx.rat = 10000;
		dry_puff = 0;
	}
}

void heat_cb_high(void)
{
	v_temp = measureVoltage();
}

void heat_cb_low(void)
{
	HEATER_A.locals.adc = getADC(&HEATER_A);
	vbat = v_temp;
}

// ============================== HEATING ==============================

void PWM_POWER_set(struct heating_packet_t *heaterPtr)
{
	setPWMdutyCycle(heaterPtr->tx.duty);
}

void startHeating(void)
{
	PWM_0_enable_output_ch2();		// Connect PWM to Output pin
	PWM_0_enable();					// Start Timer
	
	puff_start_time = getGlobalTime();
	
	heating_on = 1;
	
//	turn_off_charger();
}

void stopHeating(void)
{
	PWM_0_disable_output_ch2();		// Disconnect PWM from Output pin
	G_HTR_set_level(0);				// Turn off transistor if on
	PWM_0_disable();				// Stop Timer
	
	heating_on = 0;
	
	resetControl(&HEATER_A);
	PWM_POWER_set(&HEATER_A);
	
	calculatePuffTime();

//	turn_on_charger();
}

void resetLowRes(void)
{
	HEATER_A.tx.lowRes = 10000;
	time_since_puff = 0;
}

void heatingProcess(void)
{
	static float lowResAvg = 0;
	uint32_t V2 = 0;

	if(heating_task_timing != 0 && getGlobalTime() < heating_task_timing + task_sleep_time)
		return;
		
	heating_task_timing = getGlobalTime();
	
	if(!heating_on){		// Occurs in PWM callback when heating on
		HEATER_A.locals.adc = getADC(&HEATER_A);
		vbat = measureVoltage();
	}
	
	calculateResRat(&HEATER_A);

	if(heating_on){
		V2 = getV2(vbat);
		temperatureControl(&HEATER_A, V2);
	}
		
	else if(HEATER->locals.connected)	// pod connected, not heating
	{
		// Calculate the average off the cold res
		time_since_puff = getGlobalTime() - puff_end_time;			// Time since the last puff
			
		// Force calc the low res if is the new res is cooler than the low res
		if(time_since_puff < 80000 && puff_end_time != 0 && HEATER->tx.lowRes > HEATER->tx.res){
			puff_end_time = 0;
		}
			
		if(time_since_puff > 80000 || puff_end_time == 0)
		{
			lowResAvg = (7*lowResAvg + (float)HEATER->tx.res)/8;
			HEATER->tx.lowRes = lowResAvg + 0.5;
		}
	}

	if(getControlType() == THERMOCOUPLE_CONTROL)
	{
 		HEATER->tx.temperature = getTemperature();
	}
}
	
// -------------------- Task --------------------

void heating_task_init()
{
	AC0.DACREF = 255;							// 1:1 DAC = Bandgap
	HEATER_A.locals.pwm_channel = AN_HEATER_1;
	HEATER_A.locals.duty_max = duty_max;

	HEATER = &HEATER_A;
	
	resetControl(HEATER);
}
