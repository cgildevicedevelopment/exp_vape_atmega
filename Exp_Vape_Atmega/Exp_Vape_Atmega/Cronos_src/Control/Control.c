/*
 * Control.c
 *
 * Created: 06/07/2019 14:11:02
 *  Author: Administrator
 */ 
// ============================== CONTROL SYSTEMS ==============================

#include "heating.h"
#include "Control.h"

uint8_t control_type = THERMOCOUPLE_CONTROL;


void setControlType(uint8_t flag)
{
	control_type = flag;
}

uint8_t getControlType(void)
{
	return control_type;
}

void initControl(void)
{
	init_CONTROL_ISR();
	setControlType(CONTROL_get_level());
}

void resetControl(struct heating_packet_t *heaterPtr)
{
	heaterPtr->tx.duty = heaterPtr->locals.duty_max;
	heaterPtr->tx.power_actual = 0;
	heaterPtr->tx.I = 0;
	heaterPtr->tx.D = 0;
	heaterPtr->locals.error_prev = 0;
	heaterPtr->locals.steady_state = 0;
}

int32_t CS_PIDragon(struct heating_packet_t *heaterPtr)
{
	int32_t error = 0;
	int32_t error_dif = 0;
	int32_t target = 0;
	
	const int32_t scale_down = 4;
	
	// P
	if(control_type == RESISTANCE_CONTROL){
		error = heaterPtr->flash.R_setpoint - heaterPtr->tx.rat;
	}
	else if(control_type == THERMOCOUPLE_CONTROL){
		error = heaterPtr->flash.T_setpoint - heaterPtr->tx.temperature;
	}
	heaterPtr->tx.P = heaterPtr->flash.Kp * error;
	
	// I
	heaterPtr->tx.I += heaterPtr->flash.Ki * error;
	
	// D
	if(!heaterPtr->locals.steady_state){
		if(error <= 3){
			heaterPtr->locals.steady_state = 1;
			heaterPtr->tx.D = 0;
		}
		else{
			if(heaterPtr->locals.error_prev == 0){
				heaterPtr->locals.error_prev = 2 * error;
			}
			error_dif = heaterPtr->locals.error_prev - error;
			if(error_dif > 0){
				heaterPtr->tx.D = heaterPtr->flash.Kd * error_dif;
				heaterPtr->locals.error_prev = error;
			}
		}
	}
	
	// Power			10%:	9, 2, 0
	// Power			100deg:	400, 1, 0
	target = heaterPtr->tx.P + (heaterPtr->tx.I / scale_down) + heaterPtr->tx.D;
	
	if(target > heaterPtr->flash.power_max){
		target = heaterPtr->flash.power_max;
	}
	else if (target < 0){
		target = 0;
	}

	if(!DI_HEAT_get_level()){
		return target;
	}
	return target;
}

int32_t CS_UpsiesDownsies(struct heating_packet_t *heaterPtr)
{
	int32_t upsies = 100;
	int32_t downsies = 200;
	int32_t error = 0;
	int32_t target = 0;
	
	error = (heaterPtr->flash.R_setpoint - 10000) - heaterPtr->tx.temperature;
	
	if(error > 0){
		target = heaterPtr->tx.duty + upsies;
		if(target > 95){
			target = 95;
		}
	}
	else{
		target = heaterPtr->tx.duty - downsies;
		if (target < 2){
			target = 2;
		}
	}
	return target;
}