/*
 * myADC.c
 *
 * Created: 04/07/2019 02:06:53
 *  Author: Administrator
 */ 


#include <atmel_start.h>
#include <adc_basic.h>
#include <atomic.h>

#include "myADC.h"
#include "heating.h"

#ifndef RESISTOR_2
#define RESISTOR_2 G_RESISTOR
#endif

#define BANDGAP 0
#define VDD		1

const uint32_t pod_connected_threshold = 1000;

// ============================== ADC ==============================

void setADCchannel(uint8_t channel)
{
	if(channel == AN_BATTERY)
	{
		ADC0.CTRLC |= 0xEF;					// ADC_REFSEL_INTREF_gc
		ADC0.MUXPOS = ADC_MUXPOS_DACREF_gc;
	}
	else if(channel == AN_HEATER_1)
	{
		ADC0.CTRLC |= ADC_REFSEL_VDDREF_gc;
		ADC0.MUXPOS = ADC_MUXPOS_AIN3_gc;
	}
}

void convertADC(uint8_t channel)
{
	if(channel == AN_HEATER_1){
		G_RES_set_level(0);
	}
	
	ADC_0_start_conversion(channel);
	while (!ADC_0_is_conversion_done());

	if(channel == AN_HEATER_1){
		G_RES_set_level(1);
	}
}

uint16_t getADCvalue(void)
{
	return ADC_0_get_conversion_result();
}

uint16_t getADC(struct heating_packet_t *heaterPtr)
{
	volatile uint16_t adc_value = 0;
	
	setADCchannel(heaterPtr->locals.pwm_channel);
	convertADC(heaterPtr->locals.pwm_channel);
	adc_value = getADCvalue();
	
	return adc_value;
}

// ============================== RESISTANCE ==============================

uint8_t podConnected(uint16_t adc_value)
{
	const uint16_t pod_connected_threshold = 1000;		// adc_value = 1023 when pod not connected (internal pull-up)
	
	return (adc_value < pod_connected_threshold);
}

static uint32_t resistance_average(uint32_t new_resitance)
{
	static uint32_t res_avg = 0;
	 
	if(res_avg == 0)
	{
		res_avg = new_resitance;
	}
	
	else
	{
		res_avg = (4 * res_avg + (float)new_resitance) / 5;
		res_avg = res_avg + 0.5;
	}	
	
	return res_avg;
}

uint32_t getResistance(uint32_t adc_value, uint8_t pod_connected)
{
	const uint32_t adc_max = 1023;
	const uint32_t series_resistance = 1000;
	
    uint32_t resistance = 0;
	
	if(pod_connected){
		resistance = ((uint32_t)(series_resistance * adc_value) / (adc_max - adc_value));

		resistance = resistance_average(resistance);
	}
	
	return resistance;
}

uint32_t getRatio(uint32_t res, uint32_t lowRes)
{
	return (10000 * res) / lowRes;
}

uint32_t getDryPuff(uint32_t rat, uint32_t dry_setpoint)
{
	return (rat > dry_setpoint);
}

// ============================== BATTERY ==============================

uint32_t measureVoltage(void)
{
	const uint32_t bandgap_value = 1100;
	const uint32_t adc_max = 1023;
	volatile uint16_t adc_value = 0;
	
//	setADCchannel(AN_BATTERY);
	convertADC(AN_BATTERY);
	adc_value = getADCvalue();
	
	/* Cannot divide by zero */
	if(adc_value == 0) return -1;
	
	vbat = (uint32_t)(bandgap_value * adc_max) / adc_value;
	//vbat = 2 * (1000 * 4095) / adc_value; --> Was in BlacBox

	return vbat;
}