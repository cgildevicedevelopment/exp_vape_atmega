/***************************************************************************************************************************************************************************
FileName:			swi_phy.h
Processor:			AT90USB1287
Compiler:			GNU Toolchain (3.6.1.1750)
IDE:				Atmel Studio 7.0.1645
Author:				Erik Fasnacht
Company:			Microchip Technology, Inc.

Summary:			Header file for swi_phy.c
    
Description:		Header file for swi_phy.c
***************************************************************************************************************************************************************************/

/***************************************************************************************************************************************************************************
    (c) 2019 Microchip Technology Inc. and its subsidiaries.

    Subject to your compliance with these terms, you may use Microchip
    software and any derivatives exclusively with Microchip products. It is
    your responsibility to comply with third party license terms applicable to
    your use of third party software (including open source software) that may
    accompany Microchip software.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS
    FOR A PARTICULAR PURPOSE.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS
    SOFTWARE.
***************************************************************************************************************************************************************************/


#ifndef SWI_PHYS_H
#define SWI_PHYS_H

#include <stdint.h>																	//data type definitions
//#include "driver_init.h"
//#include "hal_delay.h"

#define swi_disable_interrupts() CRITICAL_SECTION_ENTER()						//disable interrupts
#define swi_enable_interrupts()  CRITICAL_SECTION_LEAVE()						//enable interrupts

#define INPUT	0																	//define input
#define OUTPUT	1																	//define output


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//define MCU port registers

// 	#define PORT_DDR         (DDRD)													//direction register for device id 0
// 	#define PORT_OUT         (PORTD)												//output port register for device id 0
// 	#define PORT_IN          (PIND)													//input port register for device id 0


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// error codes for physical hardware dependent module

	#define SWI_FUNCTION_RETCODE_SUCCESS			((uint8_t) 0x00)				//communication with device succeeded.
	#define SWI_FUNCTION_RETCODE_TIMEOUT			((uint8_t) 0xF1)				//communication timed out.
	#define SWI_FUNCTION_RETCODE_RX_FAIL			((uint8_t) 0xF9)				//communication failed after at least one byte was received.
	#define SWI_ADDR_NAK_FAIL						((uint8_t) 0xF2)				//NAK during address.
	#define SWI_DATA_NAK_FAIL						((uint8_t) 0xF3)				//communication failed after at least one byte was received.


//#include <peripheral_clk_config.h>
#define UM_TO_CYCLE(um) 0// (uint32_t)(((um) * (CONF_CPU_FREQUENCY / 100000.0) + 29) / 30.0)
//((1 * (freq / 100000) + 29) / 30);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//define Reset and Discovery Timing
	#define _delay_ns(ns)		_delay_cycles(NULL, UM_TO_CYCLE((((ns) - 00) / 1000.0)))  //delay_us(ns / 1000) //for (int i=0; i < 6 * (ns / 1000); i++){} //delay_us(ns / 1000)
//	#define _delay_ns(ns)       _delay_cycles((ns / 1000));
	#define ___delay_us(us)		_delay_cycles(NULL, UM_TO_CYCLE(us))  //delay_us(us) //for (int i=0; i < 6 * (us); i++){} //delay_us(us)
	#define ___delay_ms(ms)		delay_ms(ms)
	
	#define tDSCHG				200000												//min spec = 150us
	#define tRESET				500000												//min spec = 480us (STD Speed)
	#define tRRT				10000												//min spec = 8us
	#define tDRR				1000												//min spec = 1us; max spec = 2us
	#define tMSDR				2000												//min spec = 2us; max spec = 6us
	#define tHTSS				200000												//min spec = 150us
	
	#define tDACK_DLY			_delay_ns(8000)
	#define tRRT_DLY			 _delay_ns(tRRT)
	#define tDRR_DLY			_delay_ns(tDRR)
	#define tMSDR_DLY			_delay_ns(tMSDR)
	#define tDSCHG_DLY			_delay_ns(tDSCHG)
	#define tDRESET_DLY			_delay_ns(tRESET)
	#define tHTSS_DLY           _delay_ns(tHTSS)


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//define High-Speed Mode Communication Timing

	
	#define tLOW0_MIN			6000
	#define tLOW0_MAX			16000
	#define tLOW1_MIN			1000
	#define tLOW1_MAX			2000
	#define tBIT_MIN			8000												//tLOW0 + tPUP + tRCV
	#define tBIT_MAX			25000
	#define tRCV_MIN			2000
	
	#define tLWO_TYPICAL		(tLOW0_MIN + (( tLOW0_MAX - tLOW0_MIN) / 2.0))	//11000		//creates typical data timing (AVG of Min and Max)
	#define tLW1_TYPICAL	    (tLOW1_MIN + (( tLOW1_MAX - tLOW1_MIN) / 2.0))	//1500		//creates typical data timing (AVG of Min and Max)
	#define tBIT_TYPICAL		(tBIT_MIN  + (( tBIT_MAX  - tBIT_MIN)  / 2.0))		//16500		//creates typical data timing (AVG of Min and Max)

	#define tLOW0_HDLY			_delay_ns(11000)									//min spec = 6us; max spec = 16us
	#define tRD_HDLY			_delay_ns(1200)										//min spec = 1us; max spec = 2us
	#define tLOW1_HDLY			_delay_ns(1500)										//min spec = 1us; max spec = 2us
	#define tRCV0_HDLY			_delay_ns(11000)
	#define tRCV1_HDLY			_delay_ns(14000)
								
	#define tRD_DLY				_delay_ns(1200)				// 1000					//min spec = 1us; max spec = 2us
	#define tSWIN_DLY			_delay_ns(1500)				// 1000					//delay to put master strobe within sample window
								
	#define tLOW0_DLY			_delay_ns(tLWO_TYPICAL)		// 11000
	#define tLOW1_DLY			_delay_ns(tLW1_TYPICAL )		// 1500		=> 1000
								
	#define tBIT_DLY			_delay_ns(tBIT_TYPICAL)		// 16500	=>16000
	#define tRCV0_DLY			_delay_ns(tBIT_TYPICAL - tLWO_TYPICAL)	// 5500		=> 5000
	#define tRCV1_DLY			_delay_ns(tBIT_TYPICAL - tLW1_TYPICAL)	// 15000
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//define MASTER releasing and driving SI/O


#define release_si(si_pin)			0 //PORT_IOBUS->Group[(enum gpio_port)GPIO_PORT(si_pin)].DIRCLR.reg = (1U << GPIO_PIN(si_pin));	//					\
	gpio_set_pin_direction(si_pin, GPIO_DIRECTION_IN);		//\
//	gpio_set_pin_pull_mode(si_pin, GPIO_PULL_OFF);
	// <y> Pull configuration
	// <id> pad_pull_config
	// <GPIO_PULL_OFF"> Off
	// <GPIO_PULL_UP"> Pull-up
	// <GPIO_PULL_DOWN"> Pull-down
	
#define drive_si_low(si_pin)	0 //PORT_IOBUS->Group[(enum gpio_port)GPIO_PORT(si_pin)].DIRSET.reg = (1U << GPIO_PIN(si_pin));// \
gpio_set_pin_direction(si_pin, GPIO_DIRECTION_OUT);						//	\
//	gpio_set_pin_level(si_pin, 0);							\
	


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//defines the structure for the packet

	typedef struct
	{
		uint8_t dev_addr;																//SWI chip address to communicate with.
		uint8_t opcode;																	//op code
		uint8_t mem_addr;																//SWI address/commands to issue to the other chip (node).
		uint8_t mem_addr_length;														//as
		void *buffer;																	//where to find the data.		
		uint16_t wlen;																	//how many bytes do we want to write.
		uint16_t rlen;																	//how many bytes do we want to read.
		uint8_t chk_ack_only_flag;														//this flag tells the low level drive to check for ack only
	}	__attribute__ ((packed)) swi_package_t;											//ensures that swi_package_t aligns to one-byte boundaries


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//function protocol

	void    swi_enable(void);
	void    swi_set_device_id(uint8_t id);
	void    swi_set_signal_pin(uint8_t end);
	void    sendAck(void);
	void    sendNack(void);
	void    swi_start_stop_cond(void);


	uint8_t swi_send_bytes(uint8_t count, uint8_t *buffer);
	uint8_t swi_send_byte(uint8_t value);
	uint8_t swi_receive_bytes(uint8_t count, uint8_t *buffer);
	uint8_t swi_device_discovery(void);
	uint8_t swi_read(const swi_package_t *pkg );
	void TestMicroSecondsTiming();
	uint8_t swi_write(const swi_package_t *pkg );
	uint8_t swi_write_stdspeed_cmd(const swi_package_t *packet );

#endif