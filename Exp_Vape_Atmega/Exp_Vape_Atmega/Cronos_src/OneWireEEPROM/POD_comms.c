/*
 * POD_comms.c
 *
 * Created: 02/09/2020 15:40:00
 *  Author: Administrator
 */ 

#include "clock_config.h"
#include <util/delay.h>
#include <string.h>
#include "swi_com.h"
#include "swi_phy.h"
#include "heating.h"


static uint8_t device_addr = NO_DEVICE_DETECTED;							// Low nibble (slave address) of the device address byte

void writeMicrochipEEPROM(uint32_t *structPtr, uint8_t size_of_struct)
{
	static uint8_t wbuffer[64];												// Buffer for data to be written to EEPROM
	static uint8_t rbuffer[64];												// Buffer for data read from EEPROM
	const uint8_t page_size = 8;											// Max data to be written to memory array = 8 due to page size
	uint8_t pages = 0;														// Pages needed to contain all bytes
	
	if(size_of_struct > 0){
		pages = (size_of_struct - 1) / 8 + 1;								// 1->8 = 0 page_0  /  9->16 = page_1  /  17->24 = page_2
		
		memcpy(wbuffer,structPtr, size_of_struct);							// Returns pointer memcpy()
		
		for(uint8_t addr=0; addr<size_of_struct; addr+=page_size)
		{
			write_memory(device_addr, addr, page_size, &wbuffer[addr]);		// Write to EEPROM starting at address 0x00
			_delay_ms(50);
		}
//		read_memory(device_addr, 0, sizeof(rbuffer), &rbuffer[0]);//read data from EEPROM that was written
	}
}

void readMicrochipEEPROM(uint32_t *structPtr, uint8_t size_of_struct)
{
	read_memory(device_addr, 0, size_of_struct, structPtr);					// Read data from EEPROM that was written
}


void get_device_addr(void)
{
	device_addr = scan_swi_device_addr();									// Performs discovery and scan for slave address that will be used for other operations
}

void init_SWI(void)
{
	swi_init();
	device_addr = scan_swi_device_addr();									// Performs discovery and scan for slave address that will be used for other operations
	if(device_addr != NO_DEVICE_DETECTED)									// If Device Detected
	{
		readMicrochipEEPROM(&HEATER_A.flash, sizeof(HEATER_A.flash));		// Read EEPROM into Heater Flash struct
	}
}


void testWriteMicrochipEEPROM(void)
{
//	struct heating_flash_t test_flash = {.power_max=1, .R_setpoint=2, .dry_setpoint=3, .T_setpoint=4, .Kp=5, .Ki=6, .Kd=7};
// 	struct heating_flash_t pod_vars = {0, 0, 0, 0, 0, 0, 0};
		 
	swi_init();
	get_device_addr();
	if(device_addr != NO_DEVICE_DETECTED)									// If Device Detected
	{
//		writeMicrochipEEPROM(&test_flash, sizeof(test_flash));
//		readMicrochipEEPROM(&pod_vars, sizeof(pod_vars));
		writeMicrochipEEPROM(&default_flash, sizeof(default_flash));
		readMicrochipEEPROM(&HEATER_A.flash, sizeof(HEATER_A.flash));
	}
}
