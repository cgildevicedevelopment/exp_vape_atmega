/**
 * Copyright (c) 2018 Microchip Technology Inc. and its subsidiaries.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Subject to your compliance with these terms, you may use Microchip
 * software and any derivatives exclusively with  Microchip products.
 * It is your responsibility to comply with  third party license terms
 * applicable to your use of third party software (including open
 * source software) that may accompany Microchip software.
 *
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".NO WARRANTIES, WHETHER
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY
 * IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS
 * FOR A PARTICULAR PURPOSE.
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL,
 * PUNITIVE, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF
 * ANY KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF
 * MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE
 * FORESEEABLE.TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL
 * LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT
 * EXCEED THE AMOUNT OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO
 * MICROCHIP FOR THIS SOFTWARE.
 *
 * \asf_license_stop
 */
/*
 * Support and FAQ: visit <a href="http://www.microchip.com/support/hottopics.aspx"></a>
 */
#ifndef _OWI_DEFS_H_
#define _OWI_DEFS_H_

#include "clock_config.h"
#include <usart_basic.h>
/****************************************************************************
 UART patterns
****************************************************************************/

#define SWI_UART			USART1	//!< SWI usart instance.
#define SWI_UART_PIN_number 0		//!< SWI UART pin number on the port.
#define SWI_UART_PIN_bm		(1 << SWI_UART_PIN_number)	//!< UART one wire pin.
#define SWI_UART_PIN_PORT	PORTC	//!< UART the v_port of the pin.
#define SWI_UART_PINCTRL	(*(volatile uint8_t *)(&SWI_UART_PIN_PORT + 0x10 + SWI_UART_PIN_number))


#define SWI_UART_WRITE1		0xff	//!< UART Write 1 bit pattern.
#define SWI_UART_WRITE0		0x00	//!< UART Write 0 bit pattern.
#define SWI_UART_READ_BIT	0xff	//!< UART Read bit pattern.
#define SWI_UART_RESET		0x00	//!< UART Reset and Discharge bit pattern.
#define SWI_UART_PRESENCE	0xff	//!< UART Discovery after reset bit pattern.

#define SWI_TIME_HTSS		150		//!< High Time for Start/Stop Condition.

// UART baud-rate to generate the waveform for the one wire protocol.
#define SWI_UART_DEFAULT_BAUT	(uint16_t)((64L * F_CPU) / (8L  * 800000L))		//!< UART baud to communicate with the ATCS21.
#define SWI_UART_RESET_BAUT		(uint16_t)((64L * F_CPU) / (16L * 100000L))		//!< UART baud to create a reset and discharge.

#define SWI_USE_INTERNAL_PULLUP
#endif
