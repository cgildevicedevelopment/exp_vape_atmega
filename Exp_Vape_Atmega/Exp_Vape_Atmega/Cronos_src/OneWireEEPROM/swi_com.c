/***************************************************************************************************************************************************************************
FileName:			swi_com.c
Processor:			AT90USB1287
Compiler:			GNU Toolchain (3.6.1.1750)
IDE:				Atmel Studio 7.0.1645
Author:				Erik Fasnacht
Company:			Microchip Technology, Inc.

Summary:			C file for the Single-Wire protocol communication level  
    
Description:		C file for the Single-Wire protocol communication level 
***************************************************************************************************************************************************************************/

/***************************************************************************************************************************************************************************
    (c) 2019 Microchip Technology Inc. and its subsidiaries.

    Subject to your compliance with these terms, you may use Microchip
    software and any derivatives exclusively with Microchip products. It is
    your responsibility to comply with third party license terms applicable to
    your use of third party software (including open source software) that may
    accompany Microchip software.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS
    FOR A PARTICULAR PURPOSE.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS
    SOFTWARE.
***************************************************************************************************************************************************************************/

#include "swi_defs.h"
#include "swi_com.h"																//definitions for swi_com.c
#include "swi_phy.h"																//definitions for swi_phy.c
#include "clock_config.h"
#include <util/delay.h>

uint16_t baud_default;
uint16_t baud_reset;

/*! \brief Initialization of the one wire bus. (Polled UART driver) 
 *
 *  This function initializes the 1-Wire bus by configuring the UART,
 *  and the pin. (COMPATIBLE WITH ATMEGA808)
 */
void swi_init()
{
	// One wire pin enable internal pull up
	SWI_UART_PINCTRL |= PORT_PULLUPEN_bm;
	// One wire set pin as input
	SWI_UART_PIN_PORT.DIR &= ~(SWI_UART_PIN_bm);

	// Loop-back Mode Enable - enables an internal connection between the TXD pin and the USART receiver.
	SWI_UART.CTRLA = USART_LBME_bm;

	// Enable Open drain mode so line can be pulled low by slave. Enable TX and RX
	SWI_UART.CTRLB =  USART_ODME_bm | USART_RXEN_bm |  USART_TXEN_bm;

	// Calculate baud register value for 9600 baud
	// Baud rate compensated with factory stored frequency error
	// Synchronous communication without Auto-baud (Sync Field)
	// 20MHz Clock, and 3V. For different voltage and clock this should be changed
	int8_t  sigrow_value = SIGROW.OSC20ERR5V; // read signed error
	int32_t baud         = SWI_UART_RESET_BAUT;         // ideal baud rate
	baud *= (1024 + sigrow_value);            // sum resolution + error
	baud /= 1024;                             // divide by resolution
	baud_reset = (int16_t)baud;

	// Set baud rate to 115200
	// Baud rate compensated with factory stored frequency error
	// Synchronous communication without Auto-baud (Sync Field)
	// 20MHz Clock, and 3V. For different voltage and clock this should be changed
	baud = SWI_UART_DEFAULT_BAUT;            // ideal baud rate
	baud *= (1024 + sigrow_value); // sum resolution + error
	baud /= 1024;                  // divide by resolution
	baud_default = (int16_t)baud;
	SWI_UART.BAUD = baud_default; // set adjusted baud rate
}

/**
 * \brief Write the memory array
 * 
 * \param dev_addr		One wire slave address.
 * \param mem_addr		Memory address to write.
 * \param wlen			How much bytes to send (write length).
 * \param buf			Pointer to the data buffer to write.
 * 
 * \return uint8_t		Return the status of the write operation.
 */
uint8_t  write_memory(uint8_t dev_addr, uint8_t mem_addr, uint8_t wlen,uint8_t *buf)
{
	swi_package_t packet;															//build packet for the operation
		packet.opcode = EEPROM_ADDRESS;												//opcode for operation
		packet.dev_addr = dev_addr;													//slave address to communicate with
		packet.mem_addr = mem_addr;													//word address for operation

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		packet.mem_addr_length = 0x01;												//not sure this is necessary
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		packet.wlen = wlen;															//how many bytes to write
		packet.rlen = 0;															//how many bytes to read (0 for write)
		packet.buffer = buf;														//where to put the read data
		packet.chk_ack_only_flag = 0;												//this flag tells the physical level to check for ACK only

	//Perform write operation and check whether device ACKs data bytes
		if (swi_write(&packet) != SWI_SUCCESS) {	return SWI_DATA_NAK_FAIL;}		//return device didn't ACK data byte code
		_delay_ms(5);																//wait tWC for write cycle to complete
		return SWI_SUCCESS;															//return device ACK'd  all data byte(s) code
		
}

/**
 * \brief		Read the memory array
 * 
 * \param dev_addr		One wire slave address.
 * \param mem_addr		Memory address to write.
 * \param rlen			How much bytes to read (read length).
 * \param buf			Pointer to a buffer to insert the read data.
 * 
 * \return uint8_t		Return the status of the read operation.
 */
uint8_t read_memory(uint8_t dev_addr, uint8_t mem_addr, uint8_t rlen,uint8_t *buf)
{
	swi_package_t packet;									//build packet for the operation
		packet.opcode = EEPROM_ADDRESS;						//opcode for operation
		packet.dev_addr = dev_addr;							//slave address to communicate with
		packet.mem_addr = mem_addr;							//word address for operation

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		packet.mem_addr_length = 0x01;						//not sure this is necessary
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		packet.wlen = 0;									//how many bytes to write (0 for read)
		packet.rlen = rlen;									//how many bytes to read
		packet.buffer = buf;								//where to put the read data
		packet.chk_ack_only_flag = 0;						//this flag tells the physical level to check for ACK only

		
		if (swi_read(&packet) != SWI_SUCCESS)				//perform a read access & check result; 
		{	
			return SWI_ADDR_NAK_FAIL;						//return fail code if failure occurs.
		}
		
		return SWI_SUCCESS;									//return success code																				
}

/**
 * \brief		Reads the manufacture ID
 * 
 * \param dev_addr		One wire slave address.
 * \param buf			Pointer to a buffer to insert ID.
 * 
 * \return uint8_t
 */
uint8_t read_mfg_id(uint8_t dev_addr, uint8_t *buf)
{
	swi_package_t packet;															//build packet for the operation
		packet.opcode = MFGIDREAD;													//opcode for operation
		packet.dev_addr = dev_addr;													//slave address to communicate with
		packet.mem_addr = 0x00;														//word address for operation (not used for operation IE dummy value)
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		packet.mem_addr_length = 0x00;												//not sure this is necessary
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		packet.wlen = 0;															//how many bytes to write (0 for read)
		packet.rlen = 3;															//how many bytes to read
		packet.buffer = buf;														//where to put the read data
		packet.chk_ack_only_flag = 0;												//this flag tells the physical level to check for ACK only
	
	for (uint8_t ii = 0; ii < 3; ii++)												//for loop for the number of bytes to be read
	{
		if (swi_read(&packet) != SWI_SUCCESS){	return SWI_ADDR_NAK_FAIL;}			//perform a read access & check result; return fail code if failure occurs
		return SWI_SUCCESS;															//return success code
	}
	return SWI_SUCCESS;	
}

/**
 * \brief Write the security register
 * 
 * \param dev_addr		One wire slave address.
 * \param mem_addr		Memory address to write.
 * \param wlen			How much bytes to send (write length).
 * \param buf			Pointer to the data buffer to write.
 *
 * \return uint8_t		Return the status of the write operation.
 */
uint8_t  write_security_register(uint8_t dev_addr, uint8_t mem_addr, uint8_t wlen,uint8_t *buf)
{	
	swi_package_t packet;															//build packet for the operation
		packet.opcode = SECREGACCESS;												//opcode for operation
		packet.dev_addr = dev_addr;													//slave address to communicate with
		packet.mem_addr = mem_addr;													//word address for operation
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
		packet.mem_addr_length = 0x01;												//not sure this is necessary
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		packet.wlen = wlen;															//how many bytes to write
		packet.rlen = 0;															//how many bytes to read (0 for write)	
		packet.buffer = buf;														//where to put the read data
		packet.chk_ack_only_flag = 0;												//this flag tells the physical level to check for ACK only
	
	//Perform write operation and check whether device ACKs data bytes
		if (swi_write(&packet) != SWI_SUCCESS) {	return SWI_DATA_NAK_FAIL;}		//return device didn't ACK data byte code
		_delay_ms(5);																//wait tWC for write cycle to complete
		return SWI_SUCCESS;															//return device ACK'd  all data byte(s) code
}

/**
 * \brief		Read the security register.
 * 
 * \param dev_addr		One wire slave address.
 * \param mem_addr		Memory address to write.
 * \param rlen			How much bytes to read (read length).
 * \param buf			Pointer to a buffer to insert the read data.
 *  
 * \return uint8_t		Return the status of the read operation.
 */
uint8_t  read_security_register(uint8_t dev_addr, uint8_t mem_addr, uint8_t rlen, uint8_t *buf)
{
	swi_package_t packet;								//build packet for the operation
	packet.opcode = SECREGACCESS;						//opcode for operation
	packet.dev_addr = dev_addr;							//slave address to communicate with
	packet.mem_addr = mem_addr;							//word address for operation
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		packet.mem_addr_length = 0x01;					//not sure this is necessary
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		packet.wlen = 0;								//how many bytes to write (0 for read)
		packet.rlen = rlen;								//how many bytes to read
		packet.buffer = buf;							//where to put the read data
		packet.chk_ack_only_flag = 0;					//this flag tells the physical level to check for ACK only;
	
		if (swi_read(&packet) != SWI_SUCCESS)			//perform a read access & check result; 
		{	
			return SWI_ADDR_NAK_FAIL;					//return fail code if failure occurs
		}
		
		return SWI_SUCCESS;								//return success code
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//read the serial number in the security register

/**
 * \brief	Read the serial number in the security register.
 * 
 * \param dev_addr		One wire slave address.
 * \param buf			Pointer to a buffer to insert the serial number.
 * 
 * \return uint8_t		Return the status of the read operation.
 */
uint8_t  read_serial_number(uint8_t dev_addr, uint8_t *buf)
{
	swi_package_t packet;															//build packet for the operation
		packet.opcode = SECREGACCESS;												//opcode for operation
		packet.dev_addr = dev_addr;													//slave address to communicate with
		packet.mem_addr = 0x00;														//word address for operation
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		packet.mem_addr_length = 0x01;												//not sure this is necessary
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		packet.wlen = 0;															//how many bytes to write (0 for read)
		packet.rlen = 8;															//how many bytes to read
		packet.buffer = buf;														//where to put the read data
		packet.chk_ack_only_flag = 0;												//this flag tells the physical level to check for ACK only;
	

		if (swi_read(&packet) != SWI_SUCCESS){	return SWI_ADDR_NAK_FAIL;}			//perform a read access & check result; return fail code if failure occurs
		
		return SWI_SUCCESS;															//return success code
}

/**
 * \brief	Lock the security register.
 * 
 * \param dev_addr		One wire slave address.
 * \param buf			Pointer to a data buffer to write (the data byte are "don�t care" bits).
 * 
 * \return uint8_t		Return the status of the locking the register.
 */
uint8_t  lock_security_register(uint8_t dev_addr, uint8_t *buf)
{
	swi_package_t packet;															//build packet for the operation
		packet.opcode = LOCKSECREG;													//opcode for operation
		packet.dev_addr = dev_addr;													//slave address to communicate with
		packet.mem_addr = 0x60;														//word address for operation												
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		packet.mem_addr_length = 0x01;												//not sure this is necessary
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		packet.wlen = 1;															//how many bytes to write
		packet.rlen = 0;															//how many bytes to read (0 for write)
		packet.buffer = buf;														//where to put the read data
		packet.chk_ack_only_flag = 0;												//this flag tells the physical level to check for ACK only
	
	//Perform write operation and check whether device ACKs data bytes
		if (swi_write(&packet) != SWI_SUCCESS) {	return SWI_DATA_NAK_FAIL;}		//return device didn't ACK data byte code
		_delay_ms(5);																//wait tWC for write cycle to complete
		return SWI_SUCCESS;															//return device ACK'd  all data byte(s) code
}

/**
 * \brief	Check lock status of the security register
 * 
 * \param dev_addr		One wire slave address.	
 * \param buf			Pointer to a data buffer to write (the data byte are "don�t care" bits).
 * 
 * \return uint8_t		returns if the security register is locked.
 */
uint8_t  check_lock_command(uint8_t dev_addr, uint8_t *buf)
{
	uint8_t retCode = 0xFF;
	
	swi_package_t packet;															//build packet for the operation
		packet.opcode = LOCKSECREG;													//opcode for operation
		packet.dev_addr = dev_addr;													//slave address to communicate with
		packet.mem_addr = 0x60;														//word address for operation
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		packet.mem_addr_length = 0x01;												//not sure this is necessary
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		packet.wlen = 0;															//how many bytes to write (0 for read)
		packet.rlen = 0;															//how many bytes to read (0 for write)
		packet.buffer = buf;														//where to put the read data
		packet.chk_ack_only_flag = 0;												//this flag tells the physical level to check for ACK only
	
	//perform a check lock command and return whether the device ACKs
		retCode = swi_write(&packet);
		buf[0] = retCode;
		return retCode;
}

/**
 * \brief	Set write protection of a zone register, lock the register (THIS IS INVERSEBLE).
 * 
 * \param dev_addr		One wire slave address.	
 * \param mem_addr		The zone address on the ROM ZONE register.
 * \param buf			Pointer to a data buffer to write (the data byte are "don�t care" bits).
 * 
 * \return uint8_t		Return the status of the locking operation.
 */
uint8_t  writing_rom_zone_register(uint8_t dev_addr, uint8_t mem_addr, uint8_t *buf)
{
	swi_package_t packet;															//build packet for the operation
		packet.opcode = ROMZONEREGACCESS;											//opcode for operation
		packet.dev_addr = dev_addr;													//slave address to communicate with
		packet.mem_addr = mem_addr;													//word address for operation

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		packet.mem_addr_length = 0x01;												//not sure this is necessary
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		packet.wlen = 1;															//how many bytes to write
		packet.rlen = 0;															//how many bytes to read (0 for write)
		packet.buffer = buf;														//where to put the read data
		packet.chk_ack_only_flag = 0;												//this flag tells the physical level to check for ACK only
	
	//Perform write operation and check whether device ACKs data bytes
		if (swi_write(&packet) != SWI_SUCCESS) {	return SWI_DATA_NAK_FAIL;}		//return device didn't ACK data byte code
		_delay_ms(5);																//wait tWC for write cycle to complete
		return SWI_SUCCESS;															//return device ACK'd  all data byte(s) code;
}

/**
 * \brief	Determine the lock state of a zone register.
 * 
 * \param dev_addr		One wire slave address.
 * \param mem_addr		The zone address on the ROM ZONE register.
 * \param buf			The buffer to store the lock state after reading.
 * 
 * \return uint8_t		Return the status of the read operation.
 */
uint8_t  reading_rom_zone_register(uint8_t dev_addr, uint8_t mem_addr, uint8_t *buf)
{
	swi_package_t packet;															//build packet for the operation
		packet.opcode = ROMZONEREGACCESS;											//opcode for operation
		packet.dev_addr = dev_addr;													//slave address to communicate with
		packet.mem_addr = mem_addr;													//word address for operation

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		packet.mem_addr_length = 0x01;												//not sure this is necessary
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		packet.wlen = 0;															//how many bytes to write (0 for read)
		packet.rlen = 1;															//how many bytes to read
		packet.buffer = buf;														//where to put the read data
		packet.chk_ack_only_flag = 0;												//this flag tells the physical level to check for ACK only
	
	for (uint8_t ii = 0; ii < 1; ii++)												//for loop for the number of bytes to be read
	{
		if (swi_read(&packet) != SWI_SUCCESS){	return SWI_ADDR_NAK_FAIL;}			//perform a read access & check result; return fail code if failure occurs
		return SWI_SUCCESS;															//return success code
	}
	return SWI_SUCCESS;	
}

/**
 * \brief		Freeze the write protection of a zone register, prevent further changing on the ROM states (THIS IS INVERSEBLE).
 * 
 * \param dev_addr		One wire slave address.	
 * \param buf			Pointer to a data buffer to write (the data byte are "don�t care" bits).
 * 
 * \return uint8_t		Return the status of the write operation.
 */
uint8_t freeze_rom_zone_state(uint8_t dev_addr, uint8_t *buf)
{
	swi_package_t packet;															//build packet for the operation
		packet.opcode = FREEZEROMZONESTATE;											//opcode for operation
		packet.dev_addr = dev_addr;													//slave address to communicate with
		packet.mem_addr = 0x55;														//word address for operation

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		packet.mem_addr_length = 0x01;												//not sure this is necessary
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		packet.wlen = 1;															//how many bytes to write
		packet.rlen = 0;															//how many bytes to read (0 for write)
		packet.buffer = buf;														//where to put the read data
		packet.chk_ack_only_flag = 0;												//this flag tells the physical level to check for ACK only
	
	//Perform write operation and check whether device ACKs data bytes
		if (swi_write(&packet) != SWI_SUCCESS) {	return SWI_DATA_NAK_FAIL;}		//return device didn't ACK data byte code
		_delay_ms(5);																//wait tWC for write cycle to complete
		return SWI_SUCCESS;															//return device ACK'd  all data byte(s) code
}


/**
 * \brief		Check whether the device is set for standard speed communication (!!!NOT WORK)
 * 
 * \param dev_addr		One wire slave address.	
 * \param buf
 * 
 * \return uint8_t
 */
uint8_t  chk_std_speed_mode(uint8_t dev_addr, uint8_t *buf)
{
	swi_package_t packet;															//build packet for the operation
		packet.opcode = STDSPEEDMODE;												//opcode for operation
		packet.dev_addr = dev_addr;													//slave address to communicate with
		packet.mem_addr = 0x00;														//word address for operation (not used for operation IE dummy value)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		packet.mem_addr_length = 0x00;												//not sure this is necessary
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		packet.wlen = 0;															//how many bytes to write (0 for read)
		packet.rlen = 0;															//how many bytes to read (0 for write)
		packet.buffer = buf;														//where to put the read data
		packet.chk_ack_only_flag = 1;												//this flag tells the physical level to check for ACK only
	
		if (swi_read(&packet) != SWI_SUCCESS){	return SWI_ADDR_NAK_FAIL;}				//perform a read access & check result; return fail code if failure occurs
		return SWI_SUCCESS;																//return success code
}

/**
 * \brief		Check whether the device is set for high-speed communication (!!!NOT WORK)
 * 
 * \param dev_addr		One wire slave address.	
 * \param buf
 * 
 * \return uint8_t
 */
uint8_t  chk_high_speed_mode(uint8_t dev_addr, uint8_t *buf)
{
	swi_package_t packet;															//build packet for the operation
		packet.opcode = HIGHSPEEDMODE;												//opcode for operation
		packet.dev_addr = dev_addr;													//slave address to communicate with
		packet.mem_addr = 0x00;														//word address for operation (not used for operation IE dummy value)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		packet.mem_addr_length = 0x00;												//not sure this is necessary
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		packet.wlen = 0;															//how many bytes to write (0 for read)
		packet.rlen = 0;															//how many bytes to read (0 for write)
		packet.buffer = buf;														//where to put the read data
		packet.chk_ack_only_flag = 1;												//this flag tells the physical level to check for ACK only
		
		if (swi_read(&packet) != SWI_SUCCESS){	return SWI_ADDR_NAK_FAIL;}				//perform a read access & check result; return fail code if failure occurs
		return SWI_SUCCESS;																//return success code
}

/**
 * \brief		Set the device for standard speed communication
 * 
 * \param dev_addr
 * \param buf
 * 
 * \return uint8_t
 */
uint8_t  set_std_speed_mode(uint8_t dev_addr, uint8_t *buf)
{
	swi_package_t packet;															//build packet for the operation
		packet.opcode = STDSPEEDMODE;												//opcode for operation
		packet.dev_addr = dev_addr;													//slave address to communicate with
		packet.mem_addr = 0x00;														//word address for operation (not used for operation IE dummy value)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
		packet.mem_addr_length = 0x00;												//not sure this is necessary
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		packet.wlen = 0;															//how many bytes to write (0 for read)
		packet.rlen = 0;															//how many bytes to read (0 for write)
		packet.buffer = buf;														//where to put the read data
		packet.chk_ack_only_flag = 1;												//this flag tells the physical level to check for ACK only

	//Perform write operation and check whether device ACKs data bytes
		if (swi_write(&packet) != SWI_SUCCESS) {	return SWI_DATA_NAK_FAIL;}		//return device didn't ACK data byte code		
		return SWI_SUCCESS;															//return device ACK'd  all data byte(s) code
}

/**
 * \brief		Set the device for high-speed communication
 * 
 * \param dev_addr
 * \param buf
 * 
 * \return uint8_t
 */
uint8_t  set_high_speed_mode(uint8_t dev_addr, uint8_t *buf)
{
	swi_package_t packet;															//build packet for the operation
		packet.opcode = HIGHSPEEDMODE;												//opcode for operation
		packet.dev_addr = dev_addr;													//slave address to communicate with
		packet.mem_addr = 0x00;														//word address for operation (not used for operation IE dummy value)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
		packet.mem_addr_length = 0x00;												//not sure this is necessary
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		packet.wlen = 0;															//how many bytes to write (0 for read)
		packet.rlen = 0;															//how many bytes to read (0 for write)
		packet.buffer = buf;														//where to put the read data
		packet.chk_ack_only_flag = 1;												//this flag tells the physical level to check for ACK only

	//Perform write operation and check whether device ACKs data bytes
		if (swi_write(&packet) != SWI_SUCCESS) {	return SWI_DATA_NAK_FAIL;}		//return device didn't ACK data byte code
		return SWI_SUCCESS;															//return device ACK'd  all data byte(s) code
}
 
/**
 * \brief		Scan to determine slave device address
 *					check 8 available slave address.
 * 
 * \return uint8_t		return the discovery slave address if not find a slave device return 0xFF
 */
uint8_t scan_swi_device_addr()
{
	uint8_t discoveredAddr = NO_DEVICE_DETECTED;									//declares variable for the discovered slave address initially equal to error
	uint8_t errCode;																//declares variable for the error code returned
	uint8_t addr;																	//declares variable for the slave address
	
	
	for(uint8_t ii =0; ii < 8; ii++)												//creates for loop for the eight slave address combos to be sent
	{
		if(swi_device_discovery() == 1)											//perform device discovery and if the device returns an ACK to the discovery
		{
			swi_start_stop_cond();													//start condition --> GO TO swi_start_stop_cond()
			addr = ii;																//placing device address on the bus
			addr <<= 1;																//shift slave address to proper bit position (left one bit)
			addr |= 0xA0;															//add EEPROM op code and set R/W = 0
			errCode = swi_send_bytes(0x01, &addr);									//send one byte @ address of addr variable
			 
			if(errCode == 0x00)														//If the device ACKs
			{
				discoveredAddr = ii;												//set discovered slave address equal to the loop count
				discoveredAddr <<= 1;												//shift discovered slave address to proper bit position (left one bit)
				swi_start_stop_cond();												//stop condition --> GO TO swi_start_stop_cond()
				break;																//break from loop
			}
		}
	}
	return discoveredAddr;															//return discovered slave address to the main code loop
}