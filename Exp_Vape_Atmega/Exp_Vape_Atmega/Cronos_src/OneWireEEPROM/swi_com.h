/***************************************************************************************************************************************************************************
FileName:			swi_com.h
Processor:			AT90USB1287
Compiler:			GNU Toolchain (3.6.1.1750)
IDE:				Atmel Studio 7.0.1645
Author:				Erik Fasnacht
Company:			Microchip Technology, Inc.

Summary:			Header file for swi_com.c
    
Description:		Header file for swi_com.c
***************************************************************************************************************************************************************************/

/***************************************************************************************************************************************************************************
    (c) 2019 Microchip Technology Inc. and its subsidiaries.

    Subject to your compliance with these terms, you may use Microchip
    software and any derivatives exclusively with Microchip products. It is
    your responsibility to comply with third party license terms applicable to
    your use of third party software (including open source software) that may
    accompany Microchip software.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS
    FOR A PARTICULAR PURPOSE.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS
    SOFTWARE.
***************************************************************************************************************************************************************************/

#ifndef AT21CS01_SWI_H_
#define AT21CS01_SWI_H_

#include <stdint.h>

//
//define error codes
// 
#define NO_DEVICE_DETECTED					 0xFF
#define SWI_SUCCESS                           ( 0)
#define AT21CS01_SWI_SUCCESS				  ( 0)
#define AT21CS01_SWI_GENERAL_ERROR			  (-1)
#define AT21CS01_SWI_WRITE_NACK				  (-2)
#define AT21CS01_SWI_READ_NACK				  (-3)
#define AT21CS01_SWI_REG_LOCKED               (-4)
#define AT21CS01_SWI_INVALID_EEPROM_ADDRESS   (-5)
#define AT21CS01_SWI_INVALID_SIZE             (-6)
#define AT21CS01_SWI_OUT_OF_BOUNDS            (-7)

//
//define op codes
// 
#define EEPROM_ADDRESS				0x0A
#define SECREGACCESS				0x0B
#define LOCKSECREG					0x02
#define ROMZONEREGACCESS			0x07
#define FREEZEROMZONESTATE			0x01
#define MFGIDREAD					0x0C
#define STDSPEEDMODE				0x0D
#define HIGHSPEEDMODE				0x0E

//
//define word address for the various ROM zones
// 
#define ROMZONEREG_MEMZONE0			((uint8_t)0x01)
#define ROMZONEREG_MEMZONE1			((uint8_t)0x02)
#define ROMZONEREG_MEMZONE2			((uint8_t)0x04)
#define ROMZONEREG_MEMZONE3			((uint8_t)0x08)


extern uint16_t baud_default;
extern uint16_t baud_reset;


//define operations

void swi_init();
uint8_t  read_mfg_id(uint8_t dev_addr, uint8_t *buf);
uint8_t  read_security_register(uint8_t dev_addr, uint8_t parm1, uint8_t parm2, uint8_t *buf);
uint8_t  read_serial_number(uint8_t dev_addr, uint8_t *buf);
uint8_t  chk_high_speed_mode(uint8_t dev_addr, uint8_t *buf);
uint8_t  chk_std_speed_mode(uint8_t dev_addr, uint8_t *buf);
uint8_t  set_std_speed_mode(uint8_t dev_addr, uint8_t *buf);
uint8_t  set_high_speed_mode(uint8_t dev_addr, uint8_t *buf);
uint8_t  freeze_rom_zone_register(uint8_t dev_addr, uint8_t *buf);
uint8_t  reading_rom_zone_register(uint8_t dev_addr, uint8_t mem_addr, uint8_t *buf);
uint8_t  writing_rom_zone_register(uint8_t dev_addr, uint8_t mem_addr, uint8_t *buf);
uint8_t  freeze_rom_zone_state(uint8_t dev_addr, uint8_t *buf);
uint8_t  lock_security_register(uint8_t dev_addr, uint8_t *buf);
uint8_t  write_security_register(uint8_t dev_addr, uint8_t arg1, uint8_t arg2, uint8_t *buf);
uint8_t  check_lock_command(uint8_t dev_addr, uint8_t *buf);
uint8_t  read_memory(uint8_t dev_addr, uint8_t mem_addr, uint8_t rlen, uint8_t *buf);
uint8_t  write_memory(uint8_t dev_addr, uint8_t mem_addr, uint8_t wlen, uint8_t *buf);
uint8_t  write_memory_buffer(uint8_t dev_addr, uint8_t mem_addr, uint8_t wlen, uint8_t *buf);
uint8_t  scan_swi_device_addr();

#endif	// AT21CS01_SWI_H_