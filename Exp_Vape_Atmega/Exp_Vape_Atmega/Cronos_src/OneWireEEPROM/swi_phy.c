/***************************************************************************************************************************************************************************
FileName:			swi_phy.c
Processor:			AT90USB1287
Compiler:			GNU Toolchain (3.6.1.1750)
IDE:				Atmel Studio 7.0.1645
Author:				Erik Fasnacht
Company:			Microchip Technology, Inc.

Summary:			C file for the Single-Wire protocol physical level 
    
Description:		C file for the Single-Wire protocol physical level  
***************************************************************************************************************************************************************************/

/***************************************************************************************************************************************************************************
    (c) 2019 Microchip Technology Inc. and its subsidiaries.

    Subject to your compliance with these terms, you may use Microchip
    software and any derivatives exclusively with Microchip products. It is
    your responsibility to comply with third party license terms applicable to
    your use of third party software (including open source software) that may
    accompany Microchip software.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS
    FOR A PARTICULAR PURPOSE.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS
    SOFTWARE.
***************************************************************************************************************************************************************************/

#include <stdint.h>																	//data type definitions
#include "swi_defs.h"          
#include "swi_com.h"																//definitions for swi_com.c
#include "swi_phy.h"																//definitions for swi_phy.c
#include "clock_config.h"
#include <util/delay.h>

#define OWI_ACK			0x00													//defines "ACK" or acknowledge
#define is_ack(res) (res == OWI_ACK)
#define is_nack(res) (!is_ack(res))

uint8_t cmdWriteBuffer[64];															//defines array for write buffer

static inline void swi_write_bit_1();
static inline void swi_write_bit_0();
static inline uint8_t swi_read_bit();
static uint8_t swi_touch_bit(uint8_t outValue);
uint8_t swi_write_byte(uint8_t data);
uint8_t swi_read_byte();
static inline uint8_t get_Ack_Nack_response();


/**
 * \brief	Write operation.
 * 
 * \param packet		Packet to send.
 * 
 * \return uint8_t		Return write status.
 */
uint8_t swi_write(const swi_package_t *packet )
{
	uint8_t index = 0;																//variable used to determine position within the writeBuffer array
	uint8_t writeSize = 1;															//set initial value to writeSize variable
	uint8_t tmp;
	
// 	swi_start_stop_cond();
// 	tmp = (((packet->opcode<<4)|packet->dev_addr) & ~0x01);
// 	swi_write_byte(tmp);
// 	//_delay_us(5);
// 	if(packet->mem_addr_length)
// 		if(is_nack(swi_write_byte(packet->mem_addr)))
// 			return 20;
// 	//_delay_us(10);
// 	for (volatile int i = 0; i < packet->wlen ; i++)
// 	{
// 		if(is_nack(swi_write_byte(((uint8_t*)packet->buffer)[i]))) 
// 			return 56;
// 		//if(is_nack( swi_write_byte(0x03))) 
// 			//index = 1;
// 	}
// 	swi_start_stop_cond();
	
	cmdWriteBuffer[index++] = (((packet->opcode<<4)|packet->dev_addr) & ~0x01);		//load device address w/ opcode and set R/W = 0
	if(packet->mem_addr_length)														//get memory address
	{
		cmdWriteBuffer[index++] = packet->mem_addr;									//memory address
		writeSize++;																//increment the writeSize variable by 1
	}
	
	memcpy(&cmdWriteBuffer[index],packet->buffer,packet->wlen);						//load data to be written
	writeSize += packet->wlen;														//evaluate writeSize + wlen variable

	swi_start_stop_cond();															//start condition --> GO TO swi_start_stop_cond()
	if((index = swi_send_bytes(writeSize, &cmdWriteBuffer[0])) != 0) 
		return index;									//transmit data bytes to the EEPROM --> GO TO swi_send_bytes()
	swi_start_stop_cond();															//stop condition --> GO TO swi_start_stop_cond()
	return SWI_SUCCESS;																//return success code
}

/**
 * \brief	Read operation.
 * 
 * \param packet		Protocol packet. 
 * 
 * \return uint8_t		Return the read status.
 */
uint8_t swi_read(const swi_package_t *packet)
{
	uint8_t index = 0;																//variable used to determine position within the writeBuffer array
	uint8_t writeSize = 1;															//set initial value to writeSize variable
	
	cmdWriteBuffer[index++] = ((packet->opcode<<4)|packet->dev_addr);				//load device address w/ opcode
	if(packet->mem_addr_length)														//get memory word address
	{
		cmdWriteBuffer[index++] = packet->mem_addr;									//set write buffer to memory word address
		writeSize++;																//increment the writeSize variable by 1
	}

	swi_start_stop_cond();															//send start condition --> GO TO swi_start_stop_cond()

	if(packet->mem_addr_length)														//perform dummy write
	{   
		cmdWriteBuffer[0] &= 0xFE;													//bitwiseAND cmdWriteBuffer array with 0xFEh
		swi_send_bytes(writeSize, &cmdWriteBuffer[0]);								//send device address byte with R/W bit = 1 --> GO TO swi_send_bytes()
		swi_start_stop_cond();														//start condition --> GO TO swi_start_stop_cond()
	}

	cmdWriteBuffer[0] |= 0x01;														//cmdWriteBuffer ORed with 0x01 or set the R/W bit = 1 in the device address byte
	swi_send_bytes((writeSize - packet->mem_addr_length), &cmdWriteBuffer[0]);		//send device address byte --> GO TO swi_send_bytes()

	if(!packet->chk_ack_only_flag)													//if the EEPROM ACKs the device address byte
	{
		swi_receive_bytes(packet->rlen, packet->buffer);							//perform read operation --> GO TO swi_receive_bytes()
	}

	swi_start_stop_cond();															//stop condition --> GO TO swi_start_stop_cond() 
	return SWI_SUCCESS;																//return success code
}

/**
 * \brief	Sends to the EEPROM raw data.
 * 
 * \param count	 -- bytes count.
 * \param buffer -- pointer to the first byte.
 * 
 * \return uint8_t if successes returned "SWI_FUNCTION_RETCODE_SUCCESS" 
 *						else returned RetCode when sending has failed.
 */
uint8_t swi_send_bytes(uint8_t count, uint8_t *buffer)
{
	uint8_t retCode = 0;							//declares variable for the returned code if a NACK is received
	
	for (uint8_t  ii = 0; ii < count; ii++)			//for loop for number of byte to be written
	{		
		retCode = swi_write_byte(buffer[ii]);		// Send a byte to the EEPROM and return ACK or NACK
		
		if(is_nack(retCode))
		{
			// Return when the EEPROM failed to send ACK
			retCode = ii == 0 ? SWI_ADDR_NAK_FAIL : ii;
			break;
		}
		
		_delay_us(5);							//slave recovery time delay (same for logic'0' and logic '1')
	}
		
	// Return sending status -- successes or failed 
	return retCode ? retCode : SWI_FUNCTION_RETCODE_SUCCESS;
}

/**
 * \brief	Receive raw data of bytes from the EEPROM device.
 * 
 * \param count			-- Number of bytes to receive.
 * \param buffer		-- Pointer to the first byte in the buffer.
 * 
 * \return uint8_t		return read status (Always true).
 */
uint8_t swi_receive_bytes(uint8_t count, uint8_t *buffer)
{	
	memset(&buffer[0], 0, count);						//clear buffer before reading

	for (uint8_t ii = 0; ii < count; ii++)				//for loop for number of byte to be received
	{
		
		buffer[ii] = swi_read_byte();					// Reads a byte from the EEPROM device
		
		if (ii < (count-1))	{ sendAck(); }				// Send ACK except for last byte of read
	}
	
	sendNack();											//send NACK to EEPROM signaling read is complete
		
	return SWI_FUNCTION_RETCODE_SUCCESS;				//return success code
}


/*! \brief  Sends one byte of data on the 1-Wire(R) bus(es).
 *
 *  This function automates the task of sending a complete byte
 *  of data on the 1-Wire bus(es).
 *
 *  \param  data    The data to send on the bus(es).
 *
 * return	the ACK/NACK response from the device.
 */
uint8_t swi_write_byte(uint8_t data)
{
	for (uint8_t bit_mask = 0x80; bit_mask >= 1; bit_mask >>= 1)	//for loop for bit mask
	{
		if (bit_mask & data)
		{
			swi_write_bit_1();		// Sends a logic '1'
		}
		else
		{
			swi_write_bit_0();		// Sends a logic '0'
		}
	}
	
	return get_Ack_Nack_response();
}

/*! \brief  Receives one byte of data from the 1-Wire(R) bus.
 *
 *  This function automates the task of receiving a complete byte
 *  of data from the 1-Wire bus.
 *
 *  \return     The byte read from the bus.
 */
uint8_t swi_read_byte()
{
	uint8_t read_byte = 0;
	
	for (uint8_t bit_mask = 0x80; bit_mask >= 1; bit_mask >>= 1)	//for loop for bit mask
	{
		if(swi_read_bit() == 1)
		{
			read_byte |= bit_mask;
		}
	}
	
	return read_byte;
}

/************************************************************************/
/* Low level bit functions                                              */
/************************************************************************/
/**
 * \brief Send ACK signal to EEPROM device.
 * 
 * \return void
 */
void sendAck()
{
	swi_write_bit_0();
}

/**
 * \brief Send NACK signal to EEPROM device.
 * 
 * 
 * \return void
 */
void sendNack()
{
	swi_write_bit_1();
}

/**
 * \brief Receive an ACK/NACK response from the 1-Wire(R) bus
 * 
 * 		This function automates the task of receiving an ACK or NACK 
 *		signal from the 1-Wire bus.
 *		
 * \return uint8_t
 */
static inline uint8_t get_Ack_Nack_response()
{
	return swi_read_bit();
}

/**
 * \brief Reset and Discovery Response -- is used to reset the device
 *				and to perform to determine is any devices are present to the bus.
 * 
 * \return uint8_t "Discovery Response Acknowledge" return if any devices is present to the bus
 */
uint8_t swi_device_discovery()
{	
	// Reset UART receiver to clear RXC register.
	SWI_UART.CTRLB &= ~(USART_RXEN_bm);
	SWI_UART.CTRLB |= (USART_RXEN_bm);
	
	// Set the uart for normal speed and sampling.
	SWI_UART.CTRLB |= USART_RXMODE_NORMAL_gc;
	
	// Set UART Baud Rate to 9600 for Reset/Presence signaling.
	SWI_UART.BAUD = baud_reset;
	
	// Reset and discharge the AT21CS.
	swi_touch_bit(SWI_UART_RESET);
	
	// Set the uart for X2 speed.
	SWI_UART.CTRLB |= USART_RXMODE_CLK2X_gc;
	
	// Not sure this is necessary.
	_delay_us(1);
	
	uint8_t ret = swi_touch_bit(SWI_UART_PRESENCE);
	
	return (ret != SWI_UART_PRESENCE);	
}

/**
 * \brief Start/Stop condition -- All transactions begin with a Start condition,
 *				Likewise, all transactions are terminated with a Stop condition.
 * 
 * \return void
 */
void swi_start_stop_cond()
{
	_delay_us(SWI_TIME_HTSS);
}

/************************************************************************/
/* Data Input and Output Bit Frames                                                                     */
/************************************************************************/
/**
 * \brief  Write a '0' to the bus(es).
 *
 *		 Generates the waveform for transmission of a '0' bit on the 1-Wire(R) bus.
 *		  
 * \return void
 */
static inline void swi_write_bit_0()
{
	swi_touch_bit(SWI_UART_WRITE0);
}

/**
 * \brief  Write a '1' to the bus(es).
 *
 *		Generates the waveform for transmission of a '1' bit on the 1-Wire(R) bus.
 *
 * \return void
 */
static inline void swi_write_bit_1()
{
	swi_touch_bit(SWI_UART_WRITE1);
}

/*! \brief  Read a bit from the bus(es).
 *
 *  Generates the waveform for reception of a bit on the 1-Wire(R) bus(es).
 *
 *  \return the logic bit that was read on the 1-Wire bus
 */
static inline uint8_t swi_read_bit()
{
	return (swi_touch_bit(SWI_UART_READ_BIT) == SWI_UART_READ_BIT);
}

/*! \brief  Write and read one bit to/from the 1-Wire bus. (Polled UART driver)
 *
 *  Writes one bit to the bus and returns the value read from the bus.
 *
 *  \param  outValue    The value to transmit on the bus.
 *
 *  \return The value received by the UART from the bus.
 */
static uint8_t swi_touch_bit(uint8_t outValue)
{
	// Place the output value in the UART transmit buffer, and wait
	// until it is received by the UART receiver.
	SWI_UART.TXDATAL = outValue;

	while (!(SWI_UART.STATUS & USART_RXCIF_bm)) {	}
		
	// Set the UART Baud Rate back to 115200kbps when finished.
	SWI_UART.BAUD = baud_default;

	_delay_us(15);
	
	return SWI_UART.RXDATAL;
}