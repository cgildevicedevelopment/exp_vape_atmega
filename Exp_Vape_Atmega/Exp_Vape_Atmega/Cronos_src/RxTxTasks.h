/*
 * RxTxTasks.h
 *
 * Created: 1/17/2018 3:55:43 PM
 *  Author: GadielW
 */ 

/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file or main.c
 * to avoid loosing it when reconfiguring.
 */
#ifndef RxTxTasks_H
#define RxTxTasks_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
#include <stdint.h>
#include "heating.h"
/* -------------------- Rx Cases -------------------- */

#define TX_ON					'O'
#define TRANSMIT_FW_VERSION		'F'
#define TRANSMIT_TX_HEADERS		'h'
#define TRANSMIT_FLASH_HEADERS	'H'
#define TRANSMIT_FLASH_TO_GUI	'V'
#define RECEIVE_FLASH_FROM_GUI	'U'
#define ENABLE_BOOTLOADER		'B'
#define TOGGLE_CONTROL_VIA_GUI	'c'
#define TOGGLE_CHARGING_VIA_GUI	'C'
#define TOGGLE_HEATING_VIA_GUI	'p'
#define TRANSMIT_SD_DATA		'S'
#define ERASE_SD				'E'
#define THIS_IS_A_TX_PACKET		'a'
#define RESET_LOW_RES			'1'

/* -------------------- Tx PACKET -------------------- */

struct __attribute__ ((__packed__)) headers
{
	uint8_t header;
	uint8_t message_size;
	char names[230];
	uint8_t checksum;
	uint8_t stop_byte;
};

struct __attribute__ ((__packed__)) tx_vars
{
	uint32_t millis;
	uint32_t heating_on;
	uint32_t voltage;
	uint32_t control_type;
	uint32_t _is_charging;
	uint32_t t1;
};

struct __attribute__ ((__packed__)) tx_packet
{
	uint8_t header;
	uint8_t message_size;
	struct tx_vars local_tx_vars;
	struct heating_tx_t heating_tx_vars;
	uint8_t checksum;
	uint8_t stop_byte;
};


/* -------------------- Rx PACKET -------------------- */

struct __attribute__ ((__packed__)) rx_packet_t
{
	uint8_t header;
	uint8_t message_size;
	//struct heating_flash_t local_rx_vars;
	uint8_t checksum;
	uint8_t stop_byte;
};

uint8_t tx_task();
void RxTxInit();
void extract_packet(uint8_t* in_buffer1);
int8_t tx_write(uint8_t *buf, uint32_t size);
void rx_task();

#endif /* RxTxTasks_H */
