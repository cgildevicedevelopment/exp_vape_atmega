/*
 * main.h
 *
 * Created: 07/07/2020 17:17:51
 *  Author: Administrator
 */ 

#define F_CPU 20000000UL	// Clock Frequency = 20MHz

#ifndef MAIN_H_
#define MAIN_H_

#define	SET_RED()				RED_set_level(0)
#define	CLEAR_RED()				RED_set_level(1)
#define	SET_GREEN()				GREEN_set_level(0)
#define	CLEAR_GREEN()			GREEN_set_level(1)
#define	SET_BLUE()				BLUE_set_level(0)
#define	CLEAR_BLUE()			BLUE_set_level(1)

#define	SET_LED1()				LED1_set_level(1)
#define	CLEAR_LED1()			LED1_set_level(0)

void Motti_play();
void Dragon_play(void);
//#define F_CPU 10000000UL

#endif /* MAIN_H_ */