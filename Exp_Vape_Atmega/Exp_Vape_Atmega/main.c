
#include <stdint.h>

#include <atmel_start.h>
#include <atomic.h>
#include <main.h>

#include <avr/io.h>

FUSES = {FUSE0_DEFAULT, FUSE1_DEFAULT, FUSE2_DEFAULT, 0xff, 0xff, (FUSE5_DEFAULT | 0x0F), FUSE6_DEFAULT, FUSE7_DEFAULT, FUSE8_DEFAULT, LOCKBITS_DEFAULT};


int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	
	/* Replace with your application code */
	// ToDo --> main_application();
	
//	while(1){}
	Dragon_play();

	
//	Motti_play();
}
