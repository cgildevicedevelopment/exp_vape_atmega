/**
 * \file
 *
 * \brief Driver initialization.
 *
 (c) 2018 Microchip Technology Inc. and its subsidiaries.

    Subject to your compliance with these terms,you may use this software and
    any derivatives exclusively with Microchip products.It is your responsibility
    to comply with third party license terms applicable to your use of third party
    software (including open source software) that may accompany Microchip software.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 */

/*
 * Code generated by START.
 *
 * This file will be overwritten when reconfiguring your START project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */

#include "driver_init.h"
#include <system.h>

/* Configure pins and initialize registers */
void ADC_0_initialization(void)
{

	// Disable digital input buffer
	AN_HTR_set_isc(PORT_ISC_INPUT_DISABLE_gc);
	// Disable pull-up resistor
	AN_HTR_set_pull_mode(PORT_PULL_OFF);

	ADC_0_init();
}

/* configure the pins and initialize the registers */
void USART_0_initialization(void)
{

	// Set pin direction to input
	RX_set_dir(PORT_DIR_IN);

	RX_set_pull_mode(
	    // <y> Pull configuration
	    // <id> pad_pull_config
	    // <PORT_PULL_OFF"> Off
	    // <PORT_PULL_UP"> Pull-up
	    PORT_PULL_OFF);

	// Set pin direction to output

	TX_set_level(
	    // <y> Initial level
	    // <id> pad_initial_level
	    // <false"> Low
	    // <true"> High
	    false);

	TX_set_dir(PORT_DIR_OUT);

	USART_0_init();
}

void PWM_0_initialization(void)
{

	// Set pin direction to input
	G_HTR_set_dir(PORT_DIR_IN);

	G_HTR_set_pull_mode(
	    // <y> Pull configuration
	    // <id> pad_pull_config
	    // <PORT_PULL_OFF"> Off
	    // <PORT_PULL_UP"> Pull-up
	    PORT_PULL_UP);

	/* set the alternate pin mux */

	PORTMUX.TCAROUTEA |= (PORTMUX_TCA00_bm | PORTMUX_TCA01_bm);

	PWM_0_init();
}

void TIMER_0_initialization(void)
{

	TIMER_0_init();
}

/* configure the pins and initialize the registers */
void SPI_0_initialization(void)
{

	// Set pin direction to input
	MISO_set_dir(PORT_DIR_IN);

	MISO_set_pull_mode(
	    // <y> Pull configuration
	    // <id> pad_pull_config
	    // <PORT_PULL_OFF"> Off
	    // <PORT_PULL_UP"> Pull-up
	    PORT_PULL_UP);

	// Set pin direction to output

	MOSI_set_level(
	    // <y> Initial level
	    // <id> pad_initial_level
	    // <false"> Low
	    // <true"> High
	    true);

	MOSI_set_dir(PORT_DIR_OUT);

	// Set pin direction to output

	SCK_set_level(
	    // <y> Initial level
	    // <id> pad_initial_level
	    // <false"> Low
	    // <true"> High
	    false);

	SCK_set_dir(PORT_DIR_OUT);

	SPI_0_init();
}

/**
 * \brief System initialization
 */
void system_init()
{
	mcu_init();

	/* PORT setting on PA2 */

	// Set pin direction to output

	LED1_set_level(
	    // <y> Initial level
	    // <id> pad_initial_level
	    // <false"> Low
	    // <true"> High
	    false);

	LED1_set_dir(PORT_DIR_OUT);

	/* PORT setting on PA3 */

	// Set pin direction to input
	CONTROL_set_dir(PORT_DIR_IN);

	CONTROL_set_pull_mode(
	    // <y> Pull configuration
	    // <id> pad_pull_config
	    // <PORT_PULL_OFF"> Off
	    // <PORT_PULL_UP"> Pull-up
	    PORT_PULL_UP);

	/* PORT setting on PA7 */

	// Set pin direction to output

	SS_THERMOCOUPLE_set_level(
	    // <y> Initial level
	    // <id> pad_initial_level
	    // <false"> Low
	    // <true"> High
	    true);

	SS_THERMOCOUPLE_set_dir(PORT_DIR_OUT);

	/* PORT setting on PC0 */

	// Set pin direction to output

	OneWire_set_level(
	    // <y> Initial level
	    // <id> pad_initial_level
	    // <false"> Low
	    // <true"> High
	    true);

	OneWire_set_dir(PORT_DIR_OUT);

	/* PORT setting on PD1 */

	// Set pin direction to output

	G_RES_set_level(
	    // <y> Initial level
	    // <id> pad_initial_level
	    // <false"> Low
	    // <true"> High
	    true);

	G_RES_set_dir(PORT_DIR_OUT);

	/* PORT setting on PD7 */

	// Set pin direction to input
	DI_HEAT_set_dir(PORT_DIR_IN);

	DI_HEAT_set_pull_mode(
	    // <y> Pull configuration
	    // <id> pad_pull_config
	    // <PORT_PULL_OFF"> Off
	    // <PORT_PULL_UP"> Pull-up
	    PORT_PULL_UP);

	/* PORT setting on PF0 */

	// Set pin direction to output

	BLUE_set_level(
	    // <y> Initial level
	    // <id> pad_initial_level
	    // <false"> Low
	    // <true"> High
	    true);

	BLUE_set_dir(PORT_DIR_OUT);

	/* PORT setting on PF1 */

	// Set pin direction to output

	GREEN_set_level(
	    // <y> Initial level
	    // <id> pad_initial_level
	    // <false"> Low
	    // <true"> High
	    true);

	GREEN_set_dir(PORT_DIR_OUT);

	/* PORT setting on PF2 */

	// Set pin direction to output

	RED_set_level(
	    // <y> Initial level
	    // <id> pad_initial_level
	    // <false"> Low
	    // <true"> High
	    true);

	RED_set_dir(PORT_DIR_OUT);

	/* PORT setting on PF3 */

	// Set pin direction to input
	nPPR_set_dir(PORT_DIR_IN);

	nPPR_set_pull_mode(
	    // <y> Pull configuration
	    // <id> pad_pull_config
	    // <PORT_PULL_OFF"> Off
	    // <PORT_PULL_UP"> Pull-up
	    PORT_PULL_UP);

	/* PORT setting on PF4 */

	// Set pin direction to input
	nCHG_set_dir(PORT_DIR_IN);

	nCHG_set_pull_mode(
	    // <y> Pull configuration
	    // <id> pad_pull_config
	    // <PORT_PULL_OFF"> Off
	    // <PORT_PULL_UP"> Pull-up
	    PORT_PULL_UP);

	/* PORT setting on PF5 */

	// Set pin direction to output

	nEN_CHG_set_level(
	    // <y> Initial level
	    // <id> pad_initial_level
	    // <false"> Low
	    // <true"> High
	    false);

	nEN_CHG_set_dir(PORT_DIR_OUT);

	CLKCTRL_init();

	VREF_0_init();

	ADC_0_initialization();

	USART_0_initialization();

	WDT_0_init();

	PWM_0_initialization();

	TIMER_0_initialization();

	SPI_0_initialization();

	CPUINT_init();

	SLPCTRL_init();

	BOD_init();
}
